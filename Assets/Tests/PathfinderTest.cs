﻿using System.Collections;
using NUnit.Framework;
using Pathfinding;
using Tiles;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine.TestTools;

namespace Tests
{
    public class PathfinderTest : AssertionHelper
    {
        const int Size = 10;
        private const Allocator Allocator = Unity.Collections.Allocator.Persistent;

        [Test]
        public void PathNotFound()
        {
            using (var obstacles = CreateObstacles())
            {
                var tileMap = new TilesMap(Allocator, Size);
                tileMap.PopulateFromObstacle(obstacles);
                var tileEnumerator = new TilesEnumerator(tileMap);
                var pathfinder = new Pathfinder(tileEnumerator);

                var actions = new NativeList<int2>(Allocator);
                var stepsMap = new StepsMap(Allocator, Size);
                try
                {
                    var actual = pathfinder.FindPath(
                        ref actions,
                        ref stepsMap,
                        new int2(7, 0),
                        new int2(9, 0));
                    Expect(actual, EqualTo(false));
                }
                finally
                {
                    tileMap.Dispose();
                    stepsMap.Dispose();
                    actions.Dispose();
                }
            }
        }

        [Test]
        public void NarrowHallPathFound()
        {
            using (var obstacles = CreateObstacles())
            {
                var tileMap = new TilesMap(Allocator, Size);
                tileMap.PopulateFromObstacle(obstacles);
                var tileEnumerator = new TilesEnumerator(tileMap);
                var pathfinder = new Pathfinder(tileEnumerator);

                var actions = new NativeList<int2>(Allocator.Persistent);
                var stepsMap = new StepsMap(Allocator, Size);
                try
                {
                    var actual = pathfinder.FindPath(
                        ref actions,
                        ref stepsMap,
                        new int2(6, 1),
                        new int2(4, 1));
                    Expect(actual, EqualTo(false));
                }
                finally
                {
                    tileMap.Dispose();
                    stepsMap.Dispose();
                    actions.Dispose();
                }
            }
        }

        [TestCaseSource(nameof(ValidPathTargets))]
        public void PathFound(int2 target)
        {
            using (var obstacles = CreateObstacles())
            {
                var tileMap = new TilesMap(Allocator, Size);
                tileMap.PopulateFromObstacle(obstacles);
                var tileEnumerator = new TilesEnumerator(tileMap);
                var pathfinder = new Pathfinder(tileEnumerator);

                var actions = new NativeList<int2>(Allocator.Persistent);
                var stepsMap = new StepsMap(Allocator, Size);
                try
                {
                    var actual = pathfinder.FindPath(
                        ref actions,
                        ref stepsMap,
                        new int2(0, 0),
                        target);
                    Expect(actual, EqualTo(true));
                }
                finally
                {
                    stepsMap.Dispose();
                    tileMap.Dispose();
                    actions.Dispose();
                }
            }
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator PathfindingTestWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }

        private static readonly object[] ValidPathTargets =
        {
            new object[] {new int2(5, 0)},
            new object[] {new int2(6, 0)},
            new object[] {new int2(0, 5)},
        };

        private static NativeArray<TilePositionComponent> CreateObstacles()
        {
            return new NativeList<TilePositionComponent>(Size * Size, Allocator.Persistent)
            {
                new TilePositionComponent(5, 9),
                new TilePositionComponent(5, 8),
                new TilePositionComponent(5, 7),
                new TilePositionComponent(5, 5),
                new TilePositionComponent(5, 4),
                new TilePositionComponent(5, 3),
                new TilePositionComponent(5, 2),
                new TilePositionComponent(5, 1),


                new TilePositionComponent(9, 2),
                new TilePositionComponent(8, 2),
                new TilePositionComponent(8, 1),
                new TilePositionComponent(8, 0),
                
                new TilePositionComponent(3, 2),
                new TilePositionComponent(3, 1),
                
                
                new TilePositionComponent(9, 9)
            };
        }
    }
}