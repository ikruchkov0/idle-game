﻿using Factory;
using Unity.Entities;

namespace WaypointSign
{
    [InternalBufferCapacity(WorldFactorySystem.BlocksPerSize*WorldFactorySystem.BlocksPerSize)]
    public struct WaypointSignElement: IBufferElementData
    {
        public Entity WaypointSign;

        public WaypointSignElement(in Entity waypointSign)
        {
            WaypointSign = waypointSign;
        }
    }
}