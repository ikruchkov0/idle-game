﻿using Flag;
using Hybrid.Disable;
using Hybrid.Positioning;
using Prefabs;
using Unity.Entities;
using UnityEngine;

namespace WaypointSign
{
    public class WaypointPrefabAuthoring : TilePrefabAuthoring<WaypointSignComponent>
    {
        private Entity? _characterEntity;

        public Entity? CharacterEntity
        {
            get => _characterEntity;
            set
            {
                _characterEntity = value;
                UpdateCharacterEntity();
            }
        }
        
        public WaypointPrefabAuthoring()
        {
            staticOffset = new Vector3(0f, 0.1f, 0f);
        }
        
        protected override void ApplyComponents(in Entity entity, ref EntityManager entityManager)
        {
            base.ApplyComponents(entity, ref entityManager);
            entityManager.AddComponentData(entity, new HybridPositioningComponent());
            HybridDisabledComponent.Apply(ref entityManager, entity);

            UpdateCharacterEntity();
        }
        
        private void UpdateCharacterEntity()
        {
            if (Entity.HasValue && EntityManager.HasValue && _characterEntity.HasValue)
            {
                var buffer = EntityManager.Value.GetBuffer<WaypointSignElement>(_characterEntity.Value);
                buffer.Add(new WaypointSignElement(Entity.Value));
            }
        }
    }
}