﻿using Hybrid.Disable;
using Tiles;
using Unity.Entities;

namespace WaypointSign
{
    public class WaypointVisualizationSystem : SystemBase
    {
        EndSimulationEntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            _commandBufferSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();
            Entities
                .ForEach((
                    in DynamicBuffer<WaypointElement> waypoints,
                    in DynamicBuffer<WaypointSignElement> waypointSigns,
                    in int entityInQueryIndex
                ) =>
                {
                    for (var i = 0; i < waypointSigns.Length; i++)
                    {
                        var sign = waypointSigns[i];
                        if (i >= waypoints.Length - 1) // last waypoint is target
                        {
                            HideSign(ref ecb, entityInQueryIndex, sign);
                            continue;
                        }
                        var waypoint = waypoints[i];
                        ShowSign(ref ecb, entityInQueryIndex, sign, waypoint);
                    }
                    
                })
                .ScheduleParallel();

            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
        
        private static void HideSign(
            ref EntityCommandBuffer.ParallelWriter ecb,
            in int entityInQueryIndex,
            in WaypointSignElement sign)
        {
            HybridDisabledComponent.Apply(ref ecb, entityInQueryIndex, sign.WaypointSign);
            ecb.RemoveComponent<TilePositionComponent>(entityInQueryIndex, sign.WaypointSign);
        }

        private static void ShowSign(
            ref EntityCommandBuffer.ParallelWriter ecb,
            in int entityInQueryIndex,
            in WaypointSignElement sign,
            in WaypointElement waypoint)
        {
            HybridEnabledComponent.Apply(ref ecb, entityInQueryIndex, sign.WaypointSign);
            var position = new TilePositionComponent(waypoint.Pos);
            ecb.AddComponent(entityInQueryIndex, sign.WaypointSign, position);
        }
    }
}