﻿using Factory;
using Unity.Entities;
using Unity.Mathematics;

namespace WaypointSign
{
    [InternalBufferCapacity(WorldFactorySystem.BlocksPerSize*WorldFactorySystem.BlocksPerSize)]
    public readonly struct WaypointElement: IBufferElementData
    {
        public readonly int2 Pos;

        public WaypointElement(int2 pos)
        {
            Pos = pos;
        }
    }
}