﻿using Actions;
using Animation;
using Movement;
using Tiles;
using Unity.Entities;
using Unity.Mathematics;

namespace Heading
{
    [UpdateBefore(typeof(ActionSystem))]
    public class HeadToTargetSystem: SystemBase
    {
        private const float MinimalHeadingDelta = 46f;
        private const float RotationSpeed = 2f;
        private const Animations RotationAnimation = Animations.Idle;

        EndSimulationEntityCommandBufferSystem _commandBufferSystem;
        
        protected override void OnCreate()
        {
            _commandBufferSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();

            var deltaTime = Time.DeltaTime;
            
            Entities
                .WithNone<FinishedAction>()
                .ForEach((
                    ref HeadingComponent heading,
                    in Entity entity,
                    in int entityInQueryIndex,
                    in TilePositionComponent currentTile,
                    in HeadingTargetComponent headingTarget) =>
                {
                    var offset = new float3();
                    
                    if (HasComponent<TileDynamicOffsetComponent>(entity))
                    {
                        var offsetComponent = GetComponent<TileDynamicOffsetComponent>(entity);
                        offset = offsetComponent.Offset;
                    }
                    
                    var deltaPos = Mathematics.CalculatePositionDelta(headingTarget.Target, currentTile.Pos, offset);
                    var targetRotation = Mathematics.CalculateLookRotation(deltaPos).ToEuler();
                    
                    var headingDelta = targetRotation.y - heading.Value;
                    
                    // if requires rotation
                    if (math.abs(headingDelta) < MinimalHeadingDelta)
                    {
                        ecb.AddComponent(entityInQueryIndex, entity, new FinishedAction());
                        return;
                    }
                    
                    Animations currentAnimation = Animations.Idle;
                    if (HasComponent<AnimationComponent>(entity))
                    {
                        var animationComponent = GetComponent<AnimationComponent>(entity);
                        currentAnimation = animationComponent.Animation;
                    }
                                        
                    //check rotation animation
                    if (currentAnimation != RotationAnimation)
                    {
                        ecb.AddComponent(entityInQueryIndex, entity, new AnimationComponent(RotationAnimation, 0.3f));
                        return;
                    }
                    
                    Animations playingAnimation = Animations.Idle;
                    if (HasComponent<PlayingAnimationComponent>(entity))
                    {
                        var currentAnimationComponent = GetComponent<PlayingAnimationComponent>(entity);
                        playingAnimation = currentAnimationComponent.Animation;
                    }

                    // wait rotation animation
                    if (playingAnimation != RotationAnimation)
                    {
                        return;
                    }
                        
                    // rotate
                    heading.Value = heading.Value + headingDelta * RotationSpeed * deltaTime;
                })
                .Schedule();
            
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}