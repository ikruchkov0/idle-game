﻿using Actions;
using Unity.Entities;
using Unity.Mathematics;

namespace Heading
{
    public readonly struct HeadingTargetComponent: IComponentData
    {
        public readonly WAction Action;
        public readonly int2 Target;

        public HeadingTargetComponent(WAction action)
        {
            Action = action;
            Target = action.Target;
        }
    }
}