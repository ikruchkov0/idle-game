﻿using Actions;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Heading
{
    [UpdateBefore(typeof(ActionSystem))]
    public class HeadingSystem: SystemBase
    {
        EndSimulationEntityCommandBufferSystem _commandBufferSystem;
        
        protected override void OnCreate()
        {
            _commandBufferSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            base.OnCreate();
        }
        
        protected override void OnUpdate()
        {
            var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();
            
            Entities
                .WithNone<FinishedAction>()
                .ForEach((
                    ref HeadingComponent heading,
                    ref RotationEulerXYZ rotation,
                    in Entity entity) =>
                {
                    var yVal = math.radians(heading.Value);
                    if (HasComponent<StaticRotationComponent>(entity))
                    {
                        var staticRotation = GetComponent<StaticRotationComponent>(entity);
                        yVal += staticRotation.RotationEuler.y;
                    }
                    rotation.Value.y = yVal;
                })
                .Schedule();

            Entities
                .WithNone<RotationEulerXYZ>()
                .WithAll<HeadingComponent>()
                .ForEach((
                    in HeadingComponent headingComponent,
                    in Entity entity,
                    in int entityInQueryIndex) =>
                {
                    var rotationEuler = new float3(0, math.radians(headingComponent.Value), 0);
                    if (HasComponent<StaticRotationComponent>(entity))
                    {
                        var staticRotation = GetComponent<StaticRotationComponent>(entity);
                        rotationEuler = rotationEuler + staticRotation.RotationEuler;
                    }

                    ecb.AddComponent(entityInQueryIndex, entity, new RotationEulerXYZ
                    {
                        Value = rotationEuler
                    });
                })
                .Schedule();
            
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}