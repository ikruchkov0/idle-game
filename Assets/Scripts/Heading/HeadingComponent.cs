﻿using Unity.Entities;

namespace Heading
{
    public struct HeadingComponent: IComponentData
    {
        public float Value;

        public HeadingComponent(float heading)
        {
            Value = heading;
        }
    }
}