﻿using Unity.Mathematics;
using UnityEngine;

namespace Heading
{
    public static class QuaternionExtensions
    {
        public static float3 ToEuler(this quaternion quaternion)
        {
            return new Quaternion(quaternion.value.x, quaternion.value.y, quaternion.value.z, quaternion.value.w).eulerAngles;
        }
        
        public static float3 ToEulerPureVersion(this quaternion quaternion)
        {
            float4 q = quaternion.value;
            double3 res;
 
            double sinrCosp = +2.0 * (q.w * q.x + q.y * q.z);
            double cosrCosp = +1.0 - 2.0 * (q.x * q.x + q.y * q.y);
            res.x = math.atan2(sinrCosp, cosrCosp);
 
            double sinp = +2.0 * (q.w * q.y - q.z * q.x);
            if (math.abs(sinp) >= 1) {
                res.y = math.PI / 2 * math.sign(sinp);
            } else {
                res.y = math.asin(sinp);
            }
 
            double sinyCosp = +2.0 * (q.w * q.z + q.x * q.y);
            double cosyCosp = +1.0 - 2.0 * (q.y * q.y + q.z * q.z);
            res.z = math.atan2(sinyCosp, cosyCosp);
 
            return (float3) res;
        }
    }
}