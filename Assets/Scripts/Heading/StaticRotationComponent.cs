﻿using Unity.Entities;
using Unity.Mathematics;

namespace Heading
{
    public readonly struct StaticRotationComponent: IComponentData
    {
        public readonly float3 RotationEuler;

        public StaticRotationComponent(float3 rotationEuler)
        {
            RotationEuler = rotationEuler;
        }
    }
}