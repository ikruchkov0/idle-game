﻿using System;
using Actions;
using Animation;
using Heading;
using Hybrid.Animations;
using Hybrid.Navigation;
using Hybrid.Positioning;
using Hybrid.UnitAnimation;
using Hybrid.WaypointLine;
using Movement;
using Tiles;
using Units;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;
using UserInput;
using WaypointSign;

namespace Character
{
    public class CharacterAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        [FormerlySerializedAs("Speed")] public float speed = 0.8f;
        public Vector3 StaticRotation = new Vector3();
        public Animator Animator;
        public NavMeshAgent Agent;
        public LineRenderer Line;
        public UnitRenderer UnitRenderer;
        
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            var constructedUnit = UnitManager.GetConstructedUnit();
            if (constructedUnit != null)
            {
                UnitRenderer.Unit = constructedUnit;
            }
            UnitRenderer.RenderUnit();
            
            SetupUnitAnimation(entity, dstManager);
            
            dstManager.AddComponentData(entity, new HybridNavAgentComponent(Agent));
            dstManager.AddComponentData(entity, new HybridPositioningComponent());
            dstManager.AddComponentData(entity, new CharacterComponent());
            dstManager.AddComponentData(entity, new MoveSpeedComponent(speed));
            dstManager.AddComponentData(entity, new TouchableComponent());
            dstManager.AddComponentData(entity, new TileStaticOffsetComponent(0f, 0.15f, 0f));
            dstManager.AddComponentData(entity, new StaticRotationComponent(StaticRotation));
            dstManager.AddComponentData(entity, new HeadingComponent());
            dstManager.AddBuffer<WaypointSignElement>(entity);
            dstManager.AddBuffer<WaypointElement>(entity);
            dstManager.AddBuffer<WAction>(entity);
            dstManager.AddComponentData(entity, new WaypointLineComponent(Line));
        }
        
        private void SetupUnitAnimation(Entity entity, EntityManager dstManager)
        {
            if (UnitRenderer == null)
            {
                throw new Exception("UnitRenderer field is not set");
            }
            
            dstManager.AddComponentData(entity, new UnitAnimatorComponent(UnitRenderer.BodyRenderer));
            dstManager.AddComponentData(entity, new AnimationComponent(Animations.Idle, 1f));
        }

        private void SetupAnimation(Entity entity, EntityManager dstManager)
        {
            if (Animator == null)
            {
                throw new Exception("Animator field is not set");
            }
            var animationBehaviours = Animator.GetBehaviours<AnimationBehaviour>();
            foreach (var animationBehaviour in animationBehaviours)
            {
                animationBehaviour.SetEntity(entity, dstManager);
            }
            
            Animator.SetLayerWeight(0, 1f);
            Animator.SetLayerWeight(1, 0f);

            dstManager.AddComponentData(entity, new HybridAnimatorComponent(Animator));
            dstManager.AddComponentData(entity, new AnimationComponent(Animations.Idle, 1f));
        }
    }
}