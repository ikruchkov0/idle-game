﻿using UnityEngine;
using System.IO;

namespace Units
{
    public static class UnitManager
    {
        private const string CreatedUnitPath = "createdUnits";
        private const string CreatedUnitFile = "createdUnit.json";
        
        private static UnitSC ConstructedUnit;

        public static UnitSC GetConstructedUnit()
        {
            return ConstructedUnit;
        }
        
        public static void SetConstructedUnit(UnitSC unit)
        {
            ConstructedUnit = unit;
        }
        
        public static UnitSC LoadUnit()
        {
            var unitJson = File.ReadAllText(Path.Combine(Application.persistentDataPath, CreatedUnitPath, CreatedUnitFile));
            var unit = ScriptableObject.CreateInstance<UnitSC>();
            JsonUtility.FromJsonOverwrite(unitJson, unit);
            return unit;
        }
        
        public static void SaveUnit(UnitSC unit)
        {
            var pathToDir = Path.Combine(Application.persistentDataPath, CreatedUnitPath);
            if (!Directory.Exists(pathToDir))
            {
                Directory.CreateDirectory(pathToDir);
            }
            var unitJson = JsonUtility.ToJson(unit);
            File.WriteAllText(Path.Combine(pathToDir, CreatedUnitFile), unitJson);
        }
    }
}