﻿using System.Collections.Generic;
using System.Linq;
using Animation;
using Constructor;
using Hybrid.Animations;
using UnityEngine;

namespace Units
{
    public abstract class BodyPartAnimator: IUnitAnimator
    {
        protected readonly Animator Animator;

        protected readonly bool HasSpeedParam;

        public BodyPartAnimator(Animator animator)
        {
            Animator = animator;
            if (!Animator)
            {
                return;
            }

            HasSpeedParam = animator.parameters.FirstOrDefault(x => x.nameHash == HybridAnimations.Speed) != null;
            Debug.LogWarning(HasSpeedParam);
        }

        public abstract void Play(Animations animation);

        public void SetSpeed(float speed)
        {
            if (HasSpeedParam)
            {
                Animator.SetFloat(HybridAnimations.Speed, speed);
            }
        }

        public static BodyPartAnimator FromType(ConstructablePartType partType, Animator animator)
        {
            switch (partType)
            {
                case ConstructablePartType.Legs:
                    return new LegsPartAnimator(animator);
                case ConstructablePartType.Weapon:
                    return new WeaponPartAnimator(animator);
                default:
                    return new EmptyPartAnimator(animator);
            }
        }
    }
    
    public class EmptyPartAnimator: BodyPartAnimator
    {
        public EmptyPartAnimator(Animator animator) : base(animator)
        {
            if (animator)
            {
                animator.enabled = false;
            }
        }


        public override void Play(Animations animation)
        {
           
        }
    }
    
    public class WeaponPartAnimator: BodyPartAnimator
    {
        public WeaponPartAnimator(Animator animator) : base(animator)
        {
            if (animator)
            {
                animator.enabled = false;
            }
        }
        
        public override void Play(Animations animation)
        {
            
        }
    }
    
    public class LegsPartAnimator: BodyPartAnimator
    {
        public LegsPartAnimator(Animator animator) : base(animator)
        {
            
        }
        
        public override void Play(Animations animation)
        {
            var hash = HybridAnimations.GetAnimationHash(animation);
            var state = Animator.GetCurrentAnimatorStateInfo(0);
            if (state.shortNameHash == hash)
            {
                return;
            }
            Debug.LogWarning($"{state.shortNameHash} {HybridAnimations.GetAnimation(state.shortNameHash)} {hash} {animation}");
            Animator.Play(hash);
        }
    }
    
    public class BodyPartRenderer: IUnitAnimator
    {
        private readonly BodyPart _bodyPart;
        private readonly GameObject _parent;
        private readonly List<BodyPartRenderer> _renderedSlots;

        private GameObject _renderedItem;
        
        public IUnitAnimator Animator { get; private set; }
        
        public BodyPartRenderer(BodyPart bodyPart, GameObject parent)
        {
            _bodyPart = bodyPart;
            _parent = parent;
            _renderedSlots = new List<BodyPartRenderer>();
        }
        
        public void RenderPart()
        {
            var prefab = _bodyPart.Item.Prefab;
            var mountRoot = _parent.transform.Find(_bodyPart.HierarchyPath);
            var item = GameObject.Instantiate(prefab, mountRoot, false);
            _renderedItem = item;
            Animator = BodyPartAnimator.FromType(_bodyPart.PartType,  _renderedItem.GetComponent<Animator>());

            // TODO: clear old slots
            foreach (var bodyPartSlot in _bodyPart.Slots)
            {
                if (!bodyPartSlot.ItemRef.isSet)
                {
                    continue;
                }
                var renderedPart = new BodyPartRenderer(bodyPartSlot, item);
                renderedPart.RenderPart();
                _renderedSlots.Add(renderedPart);
            }
        }

        public void Play(Animations animation)
        {
            Animator.Play(animation);
            foreach (var slot in _renderedSlots)
            {
                slot.Play(animation);
            }
        }
        
        public void SetSpeed(float speed)
        {
            Animator.SetSpeed(speed);
            foreach (var slot in _renderedSlots)
            {
                slot.SetSpeed(speed);
            }
        }
    }

    public interface IUnitAnimator
    {
        void Play(Animations animation);
        void SetSpeed(float speed);
    }
    
    public class UnitRenderer: MonoBehaviour
    {
        public UnitSC Unit;

        public BodyPartRenderer BodyRenderer;

        public void RenderUnit()
        {
            if (!Unit)
            {
                return;
            }

            if (BodyRenderer != null)
            {
                Debug.LogError("Unit was already rendered");
                return;
            }

            BodyRenderer = new BodyPartRenderer(Unit.Body, gameObject);
            BodyRenderer.RenderPart();
        }
    }
}