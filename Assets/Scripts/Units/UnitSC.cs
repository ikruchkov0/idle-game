﻿using System;
using System.Collections.Generic;
using System.Linq;
using Constructor;
using UnityEditor;
using UnityEngine;

namespace Units
{

    [CustomEditor(typeof(UnitSC))]
    public class UnitSCEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Update From Prefab"))
            {
                serializedObject.Update();
                var t = (UnitSC) serializedObject.targetObject;
                EditorGUI.BeginChangeCheck();
                EditorUtility.SetDirty(t);
                t.UpdateFromPrefab();
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }

        }
    }

    [Serializable]
    public class BodyPart
    {
        public string SlotName;

        public ConstructablePartType PartType;

        public string HierarchyPath;
        
        public LazyLoadReference<ConstructableItemSC> ItemRef;

        public List<BodyPart> Slots = new List<BodyPart>();
        
        public ConstructableItemSC Item => ItemRef.asset;
        
        public void UpdateFromPrefab()
        {
            if (!ItemRef.isSet)
            {
                return;
            }

            if (Slots.Select(x => x.HierarchyPath).SequenceEqual(Item.MountPoints.Select(x => x.HierarchyPath)))
            {
                UpdateSlotsFromPrefab();
                return;
            }
            
            Slots.Clear();
            foreach (var mountPoint in Item.MountPoints)
            {
                Slots.Add(new BodyPart
                {
                    SlotName = mountPoint.Name,
                    HierarchyPath = mountPoint.HierarchyPath,
                    PartType = mountPoint.Type,
                });
            }
            UpdateSlotsFromPrefab();
        }

        private void UpdateSlotsFromPrefab()
        {
            foreach (var slot in Slots)
            {
                slot.UpdateFromPrefab();
            }
        }
    }
    
    [CreateAssetMenu(fileName = "Unit", menuName = "ScriptableObjects/UnitSC", order = 3)]
    public class UnitSC : ScriptableObject
    {
        public string Name;

        public BodyPart Body = new BodyPart
        {
            SlotName = "Platform",
            PartType = ConstructablePartType.Platform
        };
        
        public void UpdateFromPrefab()
        {
            Body.UpdateFromPrefab();
        }
    }
}