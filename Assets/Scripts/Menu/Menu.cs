﻿using Factory;
using Flag;
using Prefabs;
using Unity.Entities;
using UnityEngine;
using WaypointSign;

namespace Menu
{
    public struct TestRotate : IComponentData {}

    public class Menu : MonoBehaviour
    {
        public RackPrefabAuthoring RackPrefab;
        public WallPrefabAuthoring WallPrefab;
        public FlagPrefabAuthoring FlagPrefab;
        public WaypointPrefabAuthoring WaypointPrefab;
        public GroundPrefabAuthoring GroundPrefab;

        public void StartGame()
        {
            var prefabs = new PrefabsCollection(RackPrefab, WallPrefab, FlagPrefab, WaypointPrefab, GroundPrefab);
            var factory = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<WorldFactorySystem>();
            factory.StartGame(prefabs);
        }

        public void EndGame()
        {
            Debug.Log("Start");
        }

    }
}