﻿using Actions;
using Unity.Entities;
using Unity.Mathematics;

namespace Movement
{
    public readonly struct MovementTargetComponent: IComponentData
    {
        public readonly WAction SourceAction;
        public readonly int2 Pos;
        
        public MovementTargetComponent(WAction sourceAction)
        {
            SourceAction = sourceAction;
            Pos = SourceAction.Target;
        }
    }
}