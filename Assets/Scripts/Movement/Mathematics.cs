﻿using Tiles;
using Unity.Mathematics;

namespace Movement
{
    public static class Mathematics
    {
        public static float3 CalculatePositionDelta(int2 targetPos, int2 currentPos, float3 offset)
        {
            var target = TilePositionSystem.CalculatePosition(targetPos);
            var start = TilePositionSystem.CalculatePosition(currentPos);
            var current = start + offset;

            return target - current;
        }
        
        public static float CalculateDistance(int2 targetPos, int2 currentPos, float3 offset)
        {
            var target = TilePositionSystem.CalculatePosition(targetPos);
            var start = TilePositionSystem.CalculatePosition(currentPos);
            var current = start + offset;

            return math.distance(current, target);
        }

        public static quaternion CalculateLookRotation(float3 deltaPos)
        {
            var vector = new float3(deltaPos.x, 0f, deltaPos.z);
            return quaternion.LookRotationSafe(math.normalize(vector), math.up());
        }
        
        public static quaternion CalculateRotation(quaternion currentRotation,float4 rotationValueDelta, float rotationSpeed, float deltaTime)
        {
            var newRotationValue = currentRotation.value + rotationValueDelta*rotationSpeed*deltaTime;
            return new quaternion(newRotationValue);
        }
    }
}