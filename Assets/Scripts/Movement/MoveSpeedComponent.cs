﻿using Unity.Entities;

namespace Movement
{
    public readonly struct MoveSpeedComponent: IComponentData
    {
        public readonly float Value;

        public MoveSpeedComponent(float value)
        {
            Value = value;
        }
    }
}