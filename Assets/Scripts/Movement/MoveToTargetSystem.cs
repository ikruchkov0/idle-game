﻿using Actions;
using Animation;
using Heading;
using Tiles;
using Unity.Entities;
using Unity.Mathematics;

namespace Movement
{
    public enum MoveToTargetState
    {
        StartAnimation,
        Animating,
        Moving
    }

    public readonly struct MoveToTargetStateComponent : IComponentData
    {
        public readonly MoveToTargetState State;
        
        public MoveToTargetStateComponent(MoveToTargetState state)
        {
            State = state;
        }
    }

    [UpdateBefore(typeof(ActionSystem))]
    public class MoveToTargetSystem: SystemBase
    {
        private const float DefaultSpeed = 1f;
        private const float RotationSpeed = 2f;
        private const float MinimalDistance = 0.01f;
        private const Animations WalkingAnimation = Animations.Walk;
        
        EndSimulationEntityCommandBufferSystem _commandBufferSystem;
        
        protected override void OnCreate()
        {
            _commandBufferSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();

            var deltaTime = Time.DeltaTime;
            
            Entities
                .WithNone<FinishedAction>()
                .ForEach((
                    ref HeadingComponent heading,
                    in Entity entity,
                    in int entityInQueryIndex,
                    in TilePositionComponent currentTile,
                    in MovementTargetComponent targetComponent) =>
                {
                    var speed = DefaultSpeed;
                    var offset = new float3();
                    
                    if (HasComponent<MoveSpeedComponent>(entity))
                    {
                        var speedComponent = GetComponent<MoveSpeedComponent>(entity);
                        speed = speedComponent.Value;
                    }
                    
                    if (HasComponent<TileDynamicOffsetComponent>(entity))
                    {
                        var offsetComponent = GetComponent<TileDynamicOffsetComponent>(entity);
                        offset = offsetComponent.Offset;
                    }
                    var deltaPos = Mathematics.CalculatePositionDelta(targetComponent.Pos, currentTile.Pos, offset);
                    var distance = Mathematics.CalculateDistance(targetComponent.Pos, currentTile.Pos, offset);

                    if (distance < MinimalDistance)
                    {
                        ecb.AddComponent<FinishedAction>(entityInQueryIndex, entity);
                        return;
                    }
                    
                    var targetRotation = Mathematics.CalculateLookRotation(deltaPos).ToEuler();
                    var headingDelta = targetRotation.y - heading.Value;

                    heading.Value = heading.Value + headingDelta * RotationSpeed * deltaTime;

                    var directionNormal = math.normalize(deltaPos);
                    var newOffset = offset + directionNormal*speed*deltaTime;
                    
                    Animations currentAnimation = Animations.Idle;
                    if (HasComponent<AnimationComponent>(entity))
                    {
                        var animationComponent = GetComponent<AnimationComponent>(entity);
                        currentAnimation = animationComponent.Animation;
                    }
                                        
                    //check walking animation
                    if (currentAnimation != WalkingAnimation)
                    {
                        ecb.AddComponent(entityInQueryIndex, entity, new MoveToTargetStateComponent(MoveToTargetState.StartAnimation));
                        ecb.AddComponent(entityInQueryIndex, entity, new AnimationComponent(WalkingAnimation, speed));
                        return;
                    }
                    
                    Animations playingAnimation = Animations.Idle;
                    if (HasComponent<PlayingAnimationComponent>(entity))
                    {
                        var currentAnimationComponent = GetComponent<PlayingAnimationComponent>(entity);
                        playingAnimation = currentAnimationComponent.Animation;
                    }

                    // wait walking animation
                    if (playingAnimation != WalkingAnimation)
                    {
                        ecb.AddComponent(entityInQueryIndex, entity, new MoveToTargetStateComponent(MoveToTargetState.Animating));
                        return;
                    }
                    
                    ecb.AddComponent(entityInQueryIndex, entity, new MoveToTargetStateComponent(MoveToTargetState.Moving));
                    ecb.AddComponent(entityInQueryIndex, entity, new TileDynamicOffsetComponent(newOffset));
                })
                .Schedule();
            
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}