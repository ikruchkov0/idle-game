﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dialogue
{
    public class DialogueOptionController: MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        public Color HoverColor;
        
        public Text Text;
        
        private Button _button;

        private bool _selected;

        private Color _initialColor;

        private Action _reaction;
        
        public void Start()
        {
            _button = GetComponent<Button>();
            _initialColor = Text.color;
        }

        public void SetText(string text)
        {
            Text.text = text;
        }
        
        public void ClearText()
        {
            Text.text = String.Empty;
            _reaction = null;
        }

        public void SetReaction(Action reaction)
        {
            _reaction = reaction;
        }
        
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            Text.color = HoverColor;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            Text.color = _initialColor;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _reaction?.Invoke();
        }

        public void React()
        {
            _reaction?.Invoke();
        }
    }
}