﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Dialogue
{
    public class DialogueController : MonoBehaviour
    {
        public Text IncomingText;
        
        [FormerlySerializedAs("_options")] public DialogueOptionController[] Options;
        
        public void SetIncoming(string text)
        {
            IncomingText.text = text;
        }

        public void ShowOptions(IReadOnlyList<OptionHandler> options)
        {
            if (options.Count > Options.Length)
            {
                throw new Exception($"Desired options count {options.Count} greater than available {Options.Length}");
            }

            for (int i = 0; i < Options.Length; i++)
            {
                var opt = Options[i];

                if (i >= options.Count)
                {
                    opt.ClearText();
                    continue;
                }

                opt.SetText($"{i+1}. {options[i].Text}");
                opt.SetReaction(options[i].Action);
            }
        }

        public void Update()
        {
            if (Options.Length < 1)
            {
                return;
            }
            if (Input.GetKeyUp(KeyCode.Alpha1))
            {
                Options[0].React();
                return;
            }
            if (Options.Length < 2)
            {
                return;
            }
            if (Input.GetKeyUp(KeyCode.Alpha2))
            {
                Options[1].React();
                return;
            }
            if (Options.Length < 3)
            {
                return;
            }
            if (Input.GetKeyUp(KeyCode.Alpha3))
            {
                Options[2].React();
                return;
            }
        }
    }
}