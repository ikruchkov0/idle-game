﻿using System;

namespace Dialogue
{
    public class OptionHandler
    {
        public OptionHandler(string text, Action action)
        {
            Text = text;
            Action = action;
        }

        public string Text { get; }
            
        public Action Action { get; }
    }
}