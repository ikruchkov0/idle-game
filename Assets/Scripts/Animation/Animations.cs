﻿namespace Animation
{
    public enum Animations: uint
    {
        Idle = 0,
        Walk = 1,
        RightTurn = 2,
        IdleReady = 3
    }
}