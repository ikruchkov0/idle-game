﻿using Unity.Entities;

namespace Animation
{
    public readonly struct AnimationComponent: IComponentData
    {
        public readonly Animations Animation;
        public readonly float Speed;

        public AnimationComponent(Animations animation, float speed)
        {
            Animation = animation;
            Speed = speed;
        }
    }
}