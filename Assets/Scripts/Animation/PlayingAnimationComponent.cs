﻿using Unity.Entities;

namespace Animation
{
    public readonly struct 
        PlayingAnimationComponent: IComponentData
    {
        public readonly Animations Animation;

        public PlayingAnimationComponent(Animations animation)
        {
            Animation = animation;
        }
    }
}