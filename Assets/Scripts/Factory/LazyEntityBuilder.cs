﻿using System;
using System.Collections.Generic;
using System.Linq;
using Prefabs;
using Tiles;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UserInput;
using Material = UnityEngine.Material;

namespace Factory
{
    public class LazyEntityBuilder
    {
        private delegate void LazyAction(ref EntityManager em, in Entity e);

        private readonly List<LazyAction> _actions;

        public LazyEntityBuilder()
        {
            _actions = new List<LazyAction>();
        }

        public LazyEntityBuilder If(bool condition, Func<LazyEntityBuilder, LazyEntityBuilder> action)
            => condition ? action(this) : this;

        public LazyEntityBuilder IfNot(bool condition, Func<LazyEntityBuilder, LazyEntityBuilder> action)
            => If(!condition, action);

        public LazyEntityBuilder SetName(string name)
            => AddAction((ref EntityManager em, in Entity e) =>
            {
#if UNITY_EDITOR
                em.SetName(e, name);
#endif
            });


        public LazyEntityBuilder SetTranslation(float x, float y, float z)
            => AddAction((ref EntityManager em, in Entity e) => em.AddComponentData(e,
                new Translation
                {
                    Value = new float3(x, y, z)
                }));

        public LazyEntityBuilder SetTilePosition(int i, int j)
            => AddAction((ref EntityManager em, in Entity e) => em.AddComponentData(e,
                new TilePositionComponent(i, j)));
        
        
        public LazyEntityBuilder AddBuffer<T>()
        where T : struct, IBufferElementData
            => AddAction((ref EntityManager em, in Entity e) => em.AddBuffer<T>(e));
        
        public LazyEntityBuilder AddStruct<T>(T componentData)
            where T : struct, IComponentData
            => AddAction((ref EntityManager em, in Entity e) => em.AddComponentData(e, componentData));
        
        public LazyEntityBuilder AddClass<T>(T componentData)
            where T : class, IComponentData
            => AddAction((ref EntityManager em, in Entity e) => em.AddComponentData(e, componentData));

        public LazyEntityBuilder SetRotationEuler(float xDeg, float yDeg, float zDeg)
            => AddAction((ref EntityManager em, in Entity e) => em.AddComponentData(e,
                new RotationEulerXYZ
                {
                    Value = new float3(math.radians(xDeg), math.radians(yDeg), math.radians(zDeg))
                }));

        public LazyEntityBuilder SetTouchable()
            => AddAction((ref EntityManager em, in Entity e) => em.AddComponentData(e,
                new TouchableComponent()));
        
        public LazyEntityBuilder SetWalkable()
            => AddAction((ref EntityManager em, in Entity e) => em.AddComponentData(e,
                new WalkableComponent()));
        
        public LazyEntityBuilder SetNonWalkable()
            => AddAction((ref EntityManager em, in Entity e) => em.AddComponentData(e,
                new NonWalkableComponent()));
        
        public LazyEntityBuilder SetDisabled()
            => AddAction((ref EntityManager em, in Entity e) => em.AddComponentData(e,
                new Disabled()));

        public LazyEntityBuilder SetScale(float x, float y, float z)
            => AddAction((ref EntityManager em, in Entity e) => em.AddComponentData(e,
                new NonUniformScale
                {
                    Value = new float3(x, y, z)
                }));

        public LazyEntityBuilder SetMeshCollider(Mesh mesh)
            => AddAction((ref EntityManager em, in Entity e) =>
            {
                var vertices = new NativeArray<float3>(
                    mesh.vertices.Select(v => new float3(v.x, v.y, v.z)).ToArray(), Allocator.Temp);

                var triangles = new NativeArray<int3>(Split(mesh.triangles).ToArray(), Allocator.Temp);

                em.AddComponentData(e, new PhysicsCollider
                {
                    Value = Unity.Physics.MeshCollider.Create(vertices, triangles, CollisionFilter.Default)
                });
            });

        public LazyEntityBuilder SetRenderBounds(float3 center, float3 extents)
            => AddAction((ref EntityManager em, in Entity e) => em.AddComponentData(e,
                new RenderBounds
                {
                    Value = new AABB
                    {
                        Center = center,
                        Extents = extents
                    }
                }));

        public LazyEntityBuilder SetMeshAndMaterial(Mesh mesh, Material material)
            => AddAction((ref EntityManager em, in Entity e) => em.AddSharedComponentData(e,
                new RenderMesh
                {
                    mesh = mesh,
                    material = material
                }));

        public Entity BuildFromArchetype(ref EntityManager em, params ComponentType[] types)
        {
            var entity = em.CreateEntity(types);
            
            ApplyActions(ref em, entity);

            return entity;
        }

        public Entity BuildFromPrefab(ref EntityManager em, in Entity prefab)
        {
            var entity = em.Instantiate(prefab);
            ApplyActions(ref em, entity);
            em.RemoveComponent<PrefabComponent>(entity);
            return entity;
        }

        private void ApplyActions(ref EntityManager em, in Entity entity)
        {
            foreach (var lazyAction in _actions)
            {
                lazyAction(ref em, entity);
            }
        }

        private LazyEntityBuilder AddAction(LazyAction action)
        {
            _actions.Add(action);
            return this;
        }

        private static IEnumerable<int3> Split(IEnumerable<int> stream)
        {
            var count = 0;
            var current = new int[3];
            foreach (var i in stream)
            {
                current[count] = i;
                count++;
                if (count == 3)
                {
                    count = 0;
                    yield return new int3(current[0], current[1], current[2]);
                }
            }
        }
    }
}