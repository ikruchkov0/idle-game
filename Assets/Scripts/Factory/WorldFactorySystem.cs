﻿using System.Linq;
using Character;
using Flag;
using Hybrid.TextMesh;
using Prefabs;
using Tiles;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using WaypointSign;
using Object = UnityEngine.Object;

namespace Factory
{
    public class WorldFactorySystem : SystemBase
    {
        public const int BlocksPerSize = 10;
        public const bool ShowTileText = false;
        
        public void StartGame(PrefabsCollection prefabs)
        {
            var em = EntityManager;
            
            if (!PrefabUtils.TryGetPrefabEntity<GroundComponent>(ref em, out var groundPrefab))
            {
                return;
            }
            
            Debug.Log("Start");
            
            int[,] walls = new int[BlocksPerSize, BlocksPerSize];
            walls[5, 9] = 1;
            walls[5, 8] = 2;
            walls[5, 7] = 2;
            
            walls[5, 5] = 2;
            walls[5, 4] = 2;
            walls[5, 3] = 1;
            walls[5, 2] = 2;
            walls[5, 1] = 2;
            
            walls[9, 2] = 1;
            walls[8, 2] = 2;
            walls[8, 1] = 2;
            walls[8, 0] = 2;
            
            walls[3, 2] = 2;
            walls[3, 1] = 2;
            walls[9, 9] = 2;

            var textMeshPrefab = (GameObject)Resources.Load("Prefabs/TileText");
            if (textMeshPrefab == null)
            {
                Debug.LogError("TileTextPrefab is not found");
                return;
            }

            for (int i = 0; i < BlocksPerSize; i++)
            for (int j = 0; j < BlocksPerSize; j++)
            {
                var isWall = walls[i, j] > 0;
                var ground = GameObject.Instantiate(prefabs.Ground);
                ground.name = isWall ? $"non_walkable_ground{i}_{j}" : $"ground{i}_{j}";
                ground.I = i;
                ground.J = j;
                ground.IsWalkable = !isWall;
                ground.IsTouchable = !isWall;
                if (isWall)
                {
                    var isRack = walls[i, j] == 2;
                    if (isRack)
                    {
                        var rack = GameObject.Instantiate(prefabs.Rack);
                        rack.name = $"rack{i}_{j}";
                        rack.I = i;
                        rack.J = j;
                        continue;
                    }
                    
                    var wall = GameObject.Instantiate(prefabs.Wall);
                    wall.name = $"wall{i}_{j}";
                    wall.I = i;
                    wall.J = j;
                }

                // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                if (ShowTileText)
                {
                    var tileTextGameObject = Object.Instantiate(textMeshPrefab);
                    var textMeshAuthoring = tileTextGameObject.AddComponent<TextMeshAuthoring>();
                    textMeshAuthoring.I = i;
                    textMeshAuthoring.J = j;
                    var tileTextMeshName = $"tileText{i}_{j}";
                    tileTextGameObject.name = tileTextMeshName;
                    var textMesh = tileTextGameObject.GetComponent<TextMesh>();
                    textMesh.text = $"{i},{j}";
                    var authoring = tileTextGameObject.GetComponent<TextMeshAuthoring>();
                    authoring.Convert(ref em);
                }
            }
            
            em.DestroyEntity(groundPrefab);

            using (var characters = em
                .CreateEntityQuery(typeof(CharacterComponent))
                .ToEntityArray(Allocator.TempJob))
            {
                for(var i = 0; i < characters.Length; i ++)
                {
                    var character = characters[i];
                    em.SetName(character, $"Character_{i}");
                    
                    em.AddComponentData(character, new TilePositionComponent(0, 0));

                    var flagObject = GameObject.Instantiate(prefabs.Flag);
                    flagObject.name = $"Flag_{i}";
                    
                    flagObject.GetComponent<FlagPrefabAuthoring>().CharacterEntity = character;

                    for (var waypointIndex = 0; waypointIndex < BlocksPerSize*BlocksPerSize; waypointIndex++)
                    {
                        var waypoint = GameObject.Instantiate(prefabs.Waypoint);
                        waypoint.name = $"waypoint_{i}_{waypointIndex}";
                        waypoint.GetComponent<WaypointPrefabAuthoring>().CharacterEntity = character;
                    }
                }
            }

        }

        protected override void OnUpdate()
        {
            //_commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}