﻿using Flag;
using Prefabs;
using WaypointSign;

namespace Factory
{
    public class PrefabsCollection
    {
        public readonly RackPrefabAuthoring Rack;
        
        public readonly WallPrefabAuthoring Wall;

        public readonly FlagPrefabAuthoring Flag;
        
        public readonly WaypointPrefabAuthoring Waypoint;
        
        public readonly GroundPrefabAuthoring Ground;

        public PrefabsCollection(
            RackPrefabAuthoring rack,
            WallPrefabAuthoring wall,
            FlagPrefabAuthoring flag,
            WaypointPrefabAuthoring waypoint,
            GroundPrefabAuthoring ground)
        {
            Rack = rack;
            Wall = wall;
            Flag = flag;
            Waypoint = waypoint;
            Ground = ground;
        }
    }
}