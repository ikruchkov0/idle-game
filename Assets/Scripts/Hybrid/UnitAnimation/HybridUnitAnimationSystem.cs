﻿using Animation;
using Hybrid.Navigation;
using Unity.Entities;

namespace Hybrid.UnitAnimation
{
    [UpdateAfter(typeof(HybridNavigationSystem))]
    public class HybridUnitAnimationSystem: SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .ForEach((
                    in AnimationComponent animation,
                    in Entity entity,
                    in UnitAnimatorComponent animatorComponent) =>
                {

                    var animator = animatorComponent.Animator;
                    if (animator == null)
                    {
                        return;
                    }
                    
                    animator.SetSpeed(animation.Speed);
                    animator.Play(animation.Animation);
                })
                .WithoutBurst()
                .Run();
        }
    }
}