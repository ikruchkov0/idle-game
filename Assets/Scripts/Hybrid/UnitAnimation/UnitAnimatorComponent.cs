﻿using JetBrains.Annotations;
using Units;
using Unity.Entities;

namespace Hybrid.UnitAnimation
{
    public class UnitAnimatorComponent: IComponentData
    {
        [CanBeNull] public readonly IUnitAnimator Animator;

        public UnitAnimatorComponent()
        {
            Animator = null;
        }
        
        public UnitAnimatorComponent(IUnitAnimator animator)
        {
            Animator = animator;
        }
    }
}