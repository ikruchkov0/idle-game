﻿using Unity.Entities;

namespace Hybrid.Disable
{
    public readonly struct HybridEnabledComponent: IComponentData
    {
        public static void Apply(
            ref EntityCommandBuffer.ParallelWriter ecb,
            in int entityInQueryIndex,
            in Entity entity)
        {
            ecb.AddComponent(entityInQueryIndex, entity, new HybridEnabledComponent());
            ecb.RemoveComponent<Disabled>(entityInQueryIndex, entity);
        }
        
        public static void Apply(
            ref EntityManager entityManager,
            in Entity entity)
        {
            entityManager.AddComponentData(entity, new HybridEnabledComponent());
            entityManager.RemoveComponent<Disabled>(entity);
        }
    }
}