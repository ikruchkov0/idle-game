﻿using JetBrains.Annotations;
using Unity.Entities;
using UnityEngine;

namespace Hybrid.Disable
{
    public readonly struct HybridDisabledComponent: IComponentData
    {
        public static void Apply(
            ref EntityCommandBuffer.ParallelWriter ecb,
            in int entityInQueryIndex,
            in Entity entity)
        {
            ecb.AddComponent(entityInQueryIndex, entity, new HybridDisabledComponent());
        }
        
        public static void Apply(
            ref EntityManager entityManager,
            in Entity entity)
        {
            entityManager.AddComponentData(entity, new HybridDisabledComponent());
        }
    }
}