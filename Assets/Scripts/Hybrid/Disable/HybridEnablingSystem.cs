﻿using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Hybrid.Disable
{
    public class HybridEnablingSystem: SystemBase
    {
        protected override void OnUpdate()
        {
            var em = EntityManager;
            
            Entities
                .ForEach((
                    in HybridEnabledComponent component,
                    in Transform transform,
                    in Entity entity) =>
                {
                    if (!transform.gameObject.activeSelf)
                    {
                        transform.gameObject.SetActive(true);
                    }
                    em.RemoveComponent<HybridEnabledComponent>(entity);
                })
                .WithStructuralChanges()
                .WithoutBurst()
                .Run();
        }
    }
}