﻿using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Hybrid.Disable
{
    public class HybridDisablingSystem: SystemBase
    {
        protected override void OnUpdate()
        {
            var em = EntityManager;
            
            Entities
                .ForEach((
                    in HybridDisabledComponent component,
                    in Transform transform,
                    in Entity entity) =>
                {
                    if (transform.gameObject.activeSelf)
                    {
                        transform.gameObject.SetActive(false);
                    }

                    em.AddComponentData(entity, new Disabled());
                    em.RemoveComponent<HybridDisabledComponent>(entity);
                })
                .WithStructuralChanges()
                .WithoutBurst()
                .Run();
        }
    }
}