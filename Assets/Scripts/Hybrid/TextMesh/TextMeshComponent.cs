﻿using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Hybrid.TextMesh
{
    public readonly struct TextMeshComponent: IComponentData
    {
        public readonly FixedString64 Text;
        public readonly Color Color;

        public TextMeshComponent(FixedString64 text, Color color)
        {
            Text = text;
            Color = color;
        }
    }
}