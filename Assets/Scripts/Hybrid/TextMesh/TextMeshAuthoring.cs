﻿using Hybrid.Positioning;
using Tiles;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Serialization;

namespace Hybrid.TextMesh
{
    public class TextMeshAuthoring : MonoBehaviour
    {
        [FormerlySerializedAs("StaticOffset")]
        [SerializeField]
        public Vector3 staticOffset = new Vector3(0f, 0.1f, 0f);

        public int I;

        public int J;
        
        public Entity Convert(ref EntityManager dstManager)
        {
            var entity = dstManager.CreateEntity();
            
            dstManager.SetName(entity, name);
            
            var textMesh = GetComponent<UnityEngine.TextMesh>();
            
            if (I != -1 && J != -1)
            {
                dstManager.AddComponentData(entity, new TilePositionComponent(I, J));
            }

            var t = transform;
            var rotation = t.rotation;
            dstManager.AddComponentData(entity, new Translation
            {
                Value = new float3(t.position)
            });
            dstManager.AddComponentData(entity, new Rotation
            {
                Value = new quaternion(rotation.x, rotation.y, rotation.z, rotation.w)
            });
            dstManager.AddComponentData(entity, new TextMeshComponent(new FixedString32(textMesh.text), textMesh.color));
            dstManager.AddComponentData(entity, new HybridTextMeshComponent(textMesh));
            dstManager.AddComponentData(entity, new HybridPositioningComponent());
            dstManager.AddComponentData(entity, new TileStaticOffsetComponent(staticOffset));

            return entity;
        }
    }
}