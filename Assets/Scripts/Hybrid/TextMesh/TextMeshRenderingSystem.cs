﻿using Unity.Entities;

namespace Hybrid.TextMesh
{
    public class TextMeshRenderingSystem: SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithChangeFilter<TextMeshComponent>()
                .ForEach((
                    HybridTextMeshComponent hybridTextMeshComponent,
                    in TextMeshComponent textMeshComponent) =>
                {
                    hybridTextMeshComponent.SetText(textMeshComponent.Text.ToString());
                    hybridTextMeshComponent.SetColor(textMeshComponent.Color);
                })
                .WithoutBurst()
                .Run();
        }
    }
}