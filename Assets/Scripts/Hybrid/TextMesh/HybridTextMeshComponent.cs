﻿using Unity.Entities;
using UnityEngine;

namespace Hybrid.TextMesh
{
    public class HybridTextMeshComponent: IComponentData
    {
        private readonly UnityEngine.TextMesh _textMesh;

        public HybridTextMeshComponent()
        {
            
        }
        
        public HybridTextMeshComponent(UnityEngine.TextMesh textMesh)
        {
            _textMesh = textMesh;
        }

        public void SetText(string text)
        {
            if (_textMesh == null)
            {
                return;
            }
            
            if (_textMesh.text == text)
            {
                return;
            }
            
            _textMesh.text = text;
        }
        
        public void SetColor(Color color)
        {
            if (_textMesh == null)
            {
                return;
            }
            
            if (_textMesh.color == color)
            {
                return;
            }
            
            _textMesh.color = color;
        }
    }
}