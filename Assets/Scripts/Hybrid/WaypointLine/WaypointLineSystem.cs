﻿using Tiles;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using WaypointSign;

namespace Hybrid.WaypointLine
{
    public class WaypointLineSystem: SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .ForEach((
                    in WaypointLineComponent component,
                    in DynamicBuffer<WaypointElement> waypoints,
                    in Transform transform,
                    in Entity entity) =>
                {
                    component.SetWaypoints(transform.position, waypoints);
                })
                .WithoutBurst()
                .Run();
        }
    }
}