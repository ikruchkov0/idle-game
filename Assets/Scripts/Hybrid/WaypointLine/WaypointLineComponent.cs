﻿using System.Linq;
using JetBrains.Annotations;
using Tiles;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using WaypointSign;

namespace Hybrid.WaypointLine
{
    public class WaypointLineComponent: IComponentData
    {
        [CanBeNull] private readonly LineRenderer _line;

        public WaypointLineComponent()
        {
            
        }
        
        public WaypointLineComponent(LineRenderer line)
        {
            _line = line;
        }

        public void SetWaypoints(Vector3 start, DynamicBuffer<WaypointElement> waypoints)
        {
            if (_line == null)
            {
                return;
            }

            var pointsCount = waypoints.Length + 1;

            _line.positionCount = pointsCount;

            var points = new Vector3[pointsCount];
            points[0] = start;
            for (var i = 0; i < waypoints.Length; i++)
            {
                points[i+1] = TilePositionSystem.CalculatePosition(waypoints[i].Pos);
            }
            
            _line.SetPositions(points);
        }
    }
}