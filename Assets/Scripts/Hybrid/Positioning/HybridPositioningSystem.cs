﻿using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Hybrid.Positioning
{
    public class HybridPositioningSystem: SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithAll<HybridPositioningComponent>()
                .ForEach((
                    in Entity entity,
                    in Translation translation,
                    in Transform transform) =>
                {
                    if (HasComponent<DetachFromEntityPositionComponent>(entity))
                    {
                        return;
                    }
                    var p = new Vector3(translation.Value.x, translation.Value.y, translation.Value.z);
                    transform.position = p;
                })
                .WithoutBurst()
                .Run();
        }
    }
}