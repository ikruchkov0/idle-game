﻿using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Hybrid.Positioning
{
    public class HybridRotationSystem: SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithAll<HybridPositioningComponent>()
                .ForEach((
                    in Entity entity,
                    in RotationEulerXYZ rotation,
                    in Transform transform) =>
                {
                    if (HasComponent<DetachFromEntityPositionComponent>(entity))
                    {
                        return;
                    }
                    var r = Quaternion.Euler(rotation.Value.x, rotation.Value.y, rotation.Value.z);
                    transform.rotation = r;
                })
                .WithoutBurst()
                .Run();
        }
    }
}