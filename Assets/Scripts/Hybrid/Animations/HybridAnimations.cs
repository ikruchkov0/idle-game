﻿using System.Collections.Generic;
using UnityEngine;

namespace Hybrid.Animations
{
    public static class HybridAnimations
    {
        public static readonly int Speed = Animator.StringToHash("Speed");
        public static readonly int Walking = Animator.StringToHash(nameof(Animation.Animations.Walk));
        public static readonly int Idle = Animator.StringToHash(nameof(Animation.Animations.Idle));
        public static readonly int RightTurn = Animator.StringToHash(nameof(Animation.Animations.RightTurn));
        public static readonly int IdleReady = Animator.StringToHash(nameof(Animation.Animations.IdleReady));
        
        private static readonly IReadOnlyDictionary<int, Animation.Animations> Enums = new Dictionary<int, Animation.Animations>
        {
            [Idle] = Animation.Animations.Idle,
            [Walking] = Animation.Animations.Walk,
            [RightTurn] = Animation.Animations.RightTurn,
            [IdleReady] = Animation.Animations.IdleReady,
        };
        
        private static readonly IReadOnlyDictionary<Animation.Animations, int> AnimationHashes = new Dictionary<Animation.Animations, int>
        {
            [Animation.Animations.Idle] = Idle,
            [Animation.Animations.Walk] = Walking,
            [Animation.Animations.RightTurn] = RightTurn,
            [Animation.Animations.IdleReady] = IdleReady,
        };
        
        public static int GetAnimationHash(Animation.Animations animation)
        {
            if (!AnimationHashes.TryGetValue(animation, out var hash))
            {
                return Idle;
            }

            return hash;
        }

        public static Animation.Animations GetAnimation(int hash)
        {
            if (!Enums.TryGetValue(hash, out var animation))
            {
                return Animation.Animations.Idle;
            }

            return animation;
        }
    }
}