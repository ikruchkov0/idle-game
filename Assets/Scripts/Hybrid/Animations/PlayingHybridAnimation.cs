﻿using Unity.Entities;

namespace Hybrid.Animations
{
    public readonly struct PlayingHybridAnimation: IComponentData
    {
        public readonly int AnimationHash;

        public PlayingHybridAnimation(int animationHash)
        {
            AnimationHash = animationHash;
        }
    }
}