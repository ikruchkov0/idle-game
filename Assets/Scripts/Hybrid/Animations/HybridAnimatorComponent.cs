﻿using JetBrains.Annotations;
using Unity.Entities;
using UnityEngine;

namespace Hybrid.Animations
{
    public class HybridAnimatorComponent: IComponentData
    {
        [CanBeNull] public readonly Animator Animator;

        public HybridAnimatorComponent()
        {
            Animator = null;
        }
        
        public HybridAnimatorComponent(Animator animator)
        {
            Animator = animator;
        }
    }
}