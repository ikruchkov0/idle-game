﻿using Actions;
using Animation;
using Unity.Entities;
using UnityEngine;

namespace Hybrid.Animations
{
    [UpdateBefore(typeof(ActionSystem))]
    public class HybridAnimationSystem: SystemBase
    {
        private static readonly int Speed = Animator.StringToHash("Speed");
        
        protected override void OnUpdate()
        {
            Entities
                .ForEach((
                    in AnimationComponent animation,
                    in Entity entity,
                    in PlayingHybridAnimation hybridAnimation,
                    in HybridAnimatorComponent animatorComponent) =>
                {
                    var hash = HybridAnimations.GetAnimationHash(animation.Animation);
                    if (hybridAnimation.AnimationHash == hash)
                    {
                        return;
                    }
                    
                    var animator = animatorComponent.Animator;
                    
                    animator.SetTrigger(hash);
                    animator.SetFloat(Speed, animation.Speed);
                })
                .WithoutBurst()
                .Run();
        }
    }
}