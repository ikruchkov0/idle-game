﻿using Animation;
using Unity.Entities;
using UnityEngine;

namespace Hybrid.Animations
{
    public class AnimationBehaviour : StateMachineBehaviour
    {
        private Entity? _entity;
        private EntityManager? _entityManager;
        private int? _animationHash;
        private Animation.Animations? _animation;
        
        public void SetEntity(Entity entity, EntityManager entityManager)
        {
            _entity = entity;
            _entityManager = entityManager;
            SetComponents();
        }
        
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            var hash = stateInfo.shortNameHash;
            var animation = HybridAnimations.GetAnimation(hash);

            _animationHash = hash;
            _animation = animation;

            SetComponents();
        }
    
        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
        }

        private void SetComponents()
        {
            if (_entity == null || _animation == null || _animationHash == null || _entityManager == null)
            {
                return;
            }

            _entityManager.Value.AddComponentData(_entity.Value, new PlayingAnimationComponent(_animation.Value));
            _entityManager.Value.AddComponentData(_entity.Value, new PlayingHybridAnimation(_animationHash.Value));
        }
    }
}
