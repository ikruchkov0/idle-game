﻿using Unity.Entities;
using Unity.Mathematics;

namespace Hybrid.Navigation
{
    public readonly struct NavAgentTargetComponent: IComponentData
    {
        public readonly int2 Pos;

        public NavAgentTargetComponent(int2 pos)
        {
            Pos = pos;
        }
    }
}