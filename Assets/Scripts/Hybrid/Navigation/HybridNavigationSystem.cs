﻿using System;
using Actions;
using Animation;
using Heading;
using Hybrid.Positioning;
using Tiles;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AI;
using WaypointSign;

namespace Hybrid.Navigation
{
    [UpdateBefore(typeof(ActionSystem))]
    public class HybridNavigationSystem: SystemBase
    {
        private static readonly int Speed = Animator.StringToHash("Speed");
        private const float MinDistance = 0.001f;
        
        protected override void OnUpdate()
        {
            var em = EntityManager;
            
            Entities
                .WithAll<DetachFromEntityPositionComponent>()
                .ForEach((
                    Entity entity,
                    HybridNavAgentComponent navAgentComponent) =>
                {
                    var agent = navAgentComponent.Agent;

                    if (agent == null)
                    {
                        return;
                    }
                    
                    var transform = agent.transform;
                    var position = transform.position;

                    var tilePos = TilePositionSystem.CalculateTilePos(position);
                    em.AddComponentData(entity, new TilePositionComponent(tilePos));
                    var waypoints = em.GetBuffer<WaypointElement>(entity);
                    for (var i = 0; i < waypoints.Length; i++)
                    {
                        if (waypoints[i].Pos.Equals(tilePos))
                        {
                            waypoints.RemoveAt(i);
                            break;
                        }
                    }

                    var currentHeading = agent.transform.rotation.eulerAngles.y;
                    if (agent.remainingDistance <= MinDistance)
                    {
                        em.RemoveComponent<DetachFromEntityPositionComponent>(entity);
                        em.AddComponentData(entity, new FinishedAction());
                        em.AddComponentData(entity, new HeadingComponent(currentHeading));
                        em.AddComponentData(entity, new AnimationComponent(Animation.Animations.Idle, 1f));
                        return;
                    }
                    
                    var relativePos = agent.steeringTarget - position;
                    var targetHeading = Quaternion.LookRotation(relativePos).eulerAngles.y;

                    var headingDelta = Math.Abs(currentHeading - targetHeading);

                    if (headingDelta > 45.0f)
                    {
                        agent.acceleration = 1f;
                        agent.speed = 0.1f;
                    }
                    else
                    {
                        agent.acceleration = 8f;
                        agent.speed = 1f;
                    }

                    em.AddComponentData(entity, new AnimationComponent(Animation.Animations.Walk, agent.speed));
                })
                .WithStructuralChanges()
                .WithoutBurst()
                .Run();

            Entities
                .ForEach((
                    ref DynamicBuffer<WaypointElement> waypoints,
                    in Entity entity,
                    in HybridNavAgentComponent navAgentComponent,
                    in NavAgentTargetComponent targetComponent,
                    in Translation translation) =>
                {
                    if (navAgentComponent.Agent == null)
                    {
                        return;
                    }
                    
                    NavMeshPath path = new NavMeshPath();
                    var pos = TilePositionSystem.CalculatePosition(targetComponent.Pos);
                    pos.y = navAgentComponent.Agent.transform.position.y;

                    Debug.Log($"Calculate path {pos}");
                    
                    if (!NavMesh.CalculatePath(navAgentComponent.Agent.transform.position, pos, NavMesh.AllAreas, path))
                    {
                        Debug.LogError($"Unable to build path to {pos}");
                        return;
                    }
                    
                    waypoints.Clear();
                    for (var i = 0; i < path.corners.Length; i++)
                    {
                        var corner = path.corners[i];
                        var cornerTile = TilePositionSystem.CalculateTilePos(corner);
                        waypoints.Add(new WaypointElement(cornerTile));
                    }
                    
                    navAgentComponent.Agent.SetPath(path);

                    em.RemoveComponent<NavAgentTargetComponent>(entity);
                    em.AddComponentData(entity, new DetachFromEntityPositionComponent());
                })
                .WithStructuralChanges()
                .WithoutBurst()
                .Run();
        }
    }
}