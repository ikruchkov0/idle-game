﻿using JetBrains.Annotations;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.AI;

namespace Hybrid.Navigation
{
    public class HybridNavAgentComponent: IComponentData
    {
        [CanBeNull] public NavMeshAgent Agent;

        public bool IsStopped;
        
        public bool IsPathStale;

        public bool PathPending;

        public float3 Destination;

        public float RemainingDistance;

        public float3 PathEndPosition;
        
        public float3 NextPosition;
        
        
        public HybridNavAgentComponent()
        {
            
        }
        
        public HybridNavAgentComponent(NavMeshAgent agent)
        {
            Agent = agent;
        }

        public void UpdateFromAgent()
        {
            if (Agent == null)
            {
                return;
            }
            IsStopped = Agent.isStopped;
            IsPathStale = Agent.isPathStale;
            PathPending = Agent.pathPending;
            Destination = Agent.destination;
            RemainingDistance = Agent.remainingDistance;
            PathEndPosition = Agent.pathEndPosition;
            NextPosition = Agent.nextPosition;
        }
    }
}