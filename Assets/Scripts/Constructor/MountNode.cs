﻿using System.Collections.Generic;
using Units;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace Constructor
{
    public class MountNode
    {
        public MountNode(MountPoint mountPoint, GameObject mountRoot, SlotHandlerFactory.FactoryDelegate slotHandlerFactory)
            : this(mountPoint, mountRoot,  slotHandlerFactory(mountRoot.transform, mountPoint.Name, mountPoint.IsRequired))
        {
        }

        public MountNode(MountPoint mountPoint, GameObject mountRoot,  SlotHandlerUI slotHandlerUI)
        {
            SlotName = mountPoint.Name;
            Variants = mountPoint.Variants;
            MountPoint = mountRoot;
            IsRequired = mountPoint.IsRequired;
            HierarchyPath = mountPoint.HierarchyPath;
            Nodes = new List<MountNode>();
            OnSelected = new UnityEvent<MountNode>();
            SlotHandlerUI = slotHandlerUI;
            PartType = mountPoint.Type;
            SlotHandlerUI.OnSelected.RemoveAllListeners();
            SlotHandlerUI.OnSelected.AddListener(() => OnSelected.Invoke(this));
        }
        
        public bool IsRequired { get; }
        public string SlotName { get; }
        public string HierarchyPath { get; }
        public ConstructablePartType PartType { get; }
        public MountSlotVariantsSC Variants { get; }
        
        public GameObject MountPoint { get; }
    
        public SlotHandlerUI SlotHandlerUI { get; }
    
        public GameObject Mounted { get; private set; }

        public List<MountNode> Nodes { get; }
    
        public bool IsMounted => Mounted != null;

        private LazyLoadReference<ConstructableItemSC> _mountedItemRef;
        
        public ConstructableItemSC MountedItem { get; private set; }
    
        public UnityEvent<MountNode> OnSelected { get; }
    
        public void Mount(LazyLoadReference<ConstructableItemSC> itemRef, GameObject mounted)
        {
            _mountedItemRef = itemRef;
            MountedItem = itemRef.asset;
            Mounted = mounted;
            SlotHandlerUI.SetMounted(true);
        }

        public void Select()
        {
            if (SlotHandlerUI != null)
            {
                SlotHandlerUI.SetSelected(true);
            }
        }
    
        public void Deselect()
        {
            if (SlotHandlerUI != null)
            {
                SlotHandlerUI.SetSelected(false);
            }
        }

        public void Unmount()
        {
            SlotHandlerUI.SetMounted(false);
            Object.Destroy(Mounted);
            Mounted = null;
            MountedItem = null;
            foreach (var node in Nodes)
            {
                node.Destroy();
            }
            Nodes.Clear();
        }
    
        public void BindChildNode(MountNode child)
        {
            Nodes.Add(child);
        }

        public void Destroy()
        {
            Unmount();
            Object.Destroy(SlotHandlerUI.gameObject);
        }

        public BodyPart ToBodyPart()
        {
            var slots = new List<BodyPart>();
            foreach (var mountNode in Nodes)
            {
                if (!mountNode.IsMounted)
                {
                    continue;
                }
                
                slots.Add(mountNode.ToBodyPart());
            }
            
            return new BodyPart
            {
                SlotName = SlotName,
                ItemRef = _mountedItemRef,
                HierarchyPath = HierarchyPath,
                PartType = PartType,
                Slots = slots
            };
        }

        public bool IsValid()
        {
            if (IsRequired && !IsMounted)
            {
                return false;
            }

            foreach (var mountNode in Nodes)
            {
                if (!mountNode.IsValid())
                {
                    return false;
                }
            }
            
            return true;
        }
    }
}