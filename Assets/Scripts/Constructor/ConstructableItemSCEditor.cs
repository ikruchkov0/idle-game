﻿using UnityEditor;
using UnityEngine;

namespace Constructor
{
    [CustomEditor(typeof(ConstructableItemSC))]
    public class ConstructableItemSCEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            
            if (GUILayout.Button("Update From Prefab"))
            {
                serializedObject.Update();
                var t = (ConstructableItemSC) serializedObject.targetObject;
                EditorGUI.BeginChangeCheck();
                EditorUtility.SetDirty(t);
                t.UpdateFromPrefab();
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
            
        }
    }
}