﻿using UnityEngine;

namespace Constructor
{
    public class SlotHandlerFactory : MonoBehaviour
    {
        public delegate SlotHandlerUI FactoryDelegate(Transform pointTo, string slotName, bool isRequired);
        
        public SlotHandlerUI slotHandlerUIPrefab;
        
        public float SlotHandleHeight = 3f;

        public GameObject Parent;
        
        public SlotHandlerUI Create(Transform pointTo, string slotName, bool isRequired)
        {
            bool leftAlignment;

            var parent = Parent.transform;
            var parentPos = parent.position;
            var offset = Vector3.up*SlotHandleHeight;
            var pos = pointTo.transform.position;
            
            if (pos.x >= parentPos.x) // right
            {
                offset += Vector3.right*3f;
                leftAlignment = false;
            }
            else
            {
                offset += Vector3.left*3f; // left
                leftAlignment = true;
            }
            
            if (pos.z >= parentPos.z) // front
            {
                offset += Vector3.forward*3f;
            }
            else
            {
                offset += Vector3.back*3f; // back
            }
            
            var handler = Instantiate(slotHandlerUIPrefab.gameObject, parent);
            handler.transform.position = pos;
            var result = handler.GetComponent<SlotHandlerUI>();
            result.Setup(slotName, offset, leftAlignment, isRequired);
            return result;
        }
    }
}