using UnityEngine;

namespace Constructor
{
    [CreateAssetMenu(fileName = "MountSlotVariants", menuName = "ScriptableObjects/MountSlotVariantsSC", order = 1)]
    public class MountSlotVariantsSC: ScriptableObject
    {
        public LazyLoadReference<ConstructableItemSC>[] VariantsRefs;
    }
}
