﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Constructor
{
    public enum ConstructablePartType
    {
        Unknown,
        Weapon,
        Cockpit,
        Legs,
        Platform,
        Shoulder,
        Backpack,
        Antenna
    }
    
    [Serializable]
    public class ConstructablePartTypeSettings
    {
        public string OriginalSlotName;
        
        public ConstructablePartType Type;

        public string DisplaySlotName;

        public bool IsRequired;

        public LazyLoadReference<MountSlotVariantsSC> VariantsRef;
    }
    
    
    [CreateAssetMenu(fileName = "ConstructablePartsMatcher", menuName = "ScriptableObjects/ConstructablePartsMatcher", order = 4)]
    public class ConstructablePartsMatcher: ScriptableObject
    {
        public List<ConstructablePartTypeSettings> PartSettings;
        
        public bool TryMatchPartSettings(string slotName, out ConstructablePartTypeSettings result)
        {
            slotName = slotName.ToLowerInvariant();
            var aliasesDictionary = new Dictionary<string, ConstructablePartTypeSettings>();

            foreach (var setting in PartSettings)
            {
                aliasesDictionary.Add(setting.OriginalSlotName.ToLowerInvariant(), setting);
            }

            return aliasesDictionary.TryGetValue(slotName, out result);
        }
    }
}