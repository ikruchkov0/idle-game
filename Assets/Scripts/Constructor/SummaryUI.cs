﻿using System.Text;
using UnityEngine.UIElements;

namespace Constructor
{
    public class SummaryUI
    {
        private Label _txtItemName;
        private Label _txtItemFirepower;
        private Label _txtItemDefense;
        private Label _txtItemFeatures;
        private Label _txtSummaryFirepower;
        private Label _txtSummaryDefense;
        private Label _txtSummaryFeatures;

        private VisualElement _itemDetailsPanel;
        private VisualElement _itemFirepowerPanel;
        private VisualElement _itemDefensePanel;
        private VisualElement _itemFeaturesPanel;

        public SummaryUI(VisualElement root)
        {
            _txtItemName = root.Q<Label>("txtItemName");
            _txtItemFirepower = root.Q<Label>("txtItemFirepower");
            _txtItemDefense = root.Q<Label>("txtItemDefense");
            _txtItemFeatures = root.Q<Label>("txtItemFeatures");
            _txtSummaryFirepower = root.Q<Label>("txtSummaryFirepower");
            _txtSummaryDefense = root.Q<Label>("txtSummaryDefense");
            _txtSummaryFeatures = root.Q<Label>("txtSummaryFeatures");
            _itemDetailsPanel = root.Q<VisualElement>("ItemDetailsPanel");
            _itemFirepowerPanel = root.Q<VisualElement>("ItemFirepowerPanel");
            _itemDefensePanel = root.Q<VisualElement>("ItemDefensePanel");
            _itemFeaturesPanel = root.Q<VisualElement>("ItemFeaturesPanel");

        }
        
        public void UpdateCurrent(ConstructableItemSC item)
        {
            Hide(_itemDetailsPanel);
            Hide(_itemFirepowerPanel);
            Hide(_itemDefensePanel);
            Hide(_itemFeaturesPanel);
            
            if (item == null)
            {
                _txtItemName.text = string.Empty;
                _txtItemFirepower.text = string.Empty;
                _txtItemDefense.text = string.Empty;
                _txtItemFeatures.text = string.Empty;
                return;
            }

            Show(_itemDetailsPanel);
            _txtItemName.text = item.Name;

            var descriptor = item.Descriptor;
            if (descriptor != null)
            {
                if (descriptor.Firepower > 0)
                {
                    Show(_itemFirepowerPanel);
                    _txtItemFirepower.text = descriptor.Firepower.ToString();
                }

                if (descriptor.Defense > 0)
                {
                    Show(_itemDefensePanel);
                    _txtItemDefense.text = descriptor.Defense.ToString();
                }

                if (!string.IsNullOrEmpty(descriptor.Feature))
                {
                    Show(_itemFeaturesPanel);
                    _txtItemFeatures.text = descriptor.Feature;
                }
            }
        }

        public void UpdateSummary(MountNode node)
        {
            var features = new StringBuilder();
            var (firepower, defense) = CollectSummary(node, features);
            
            _txtSummaryFirepower.text = firepower.ToString();
            _txtSummaryDefense.text = defense.ToString();
            _txtSummaryFeatures.text = features.ToString();
        }
        
        public (int firepower, int defense) CollectSummary(MountNode node, StringBuilder features)
        {
            var firepower = 0;
            var defense = 0;
            if (node.MountedItem == null)
            {
                return (firepower, defense);
            }

            var descriptor = node.MountedItem.Descriptor;
            if (descriptor != null)
            {
                firepower += descriptor.Firepower;
                defense += descriptor.Defense;
                if (!string.IsNullOrEmpty(descriptor.Feature))
                {
                    features.AppendLine(descriptor.Feature);
                }
            }

            foreach (var child in node.Nodes)
            {
                var (f, d) = CollectSummary(child, features);
                firepower += f;
                defense += d;
            }
            
            return (firepower, defense);
        }

        private void Hide(VisualElement el)
        {
            el.AddToClassList("hidden");
        }
        
        private void Show(VisualElement el)
        {
            el.RemoveFromClassList("hidden");
        }
    }
}