﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;
using Button = UnityEngine.UIElements.Button;

namespace Constructor
{
    public delegate void ConstructorDropdownUICallback(int value);
    
    public class DropdownUI
    {
        private readonly VisualElement _dropdown;
        private readonly Label _txtSlotName;
        private readonly List<Button> _buttons;

        public DropdownUI(VisualElement root)
        {
            _dropdown = root.Q<VisualElement>("VariantsList");
            _txtSlotName = root.Q<Label>("txtSlotName");
            _buttons = new List<Button>();
        }
        
        public void SetupSlot(string slotName, MountSlotVariantsSC variants, MountNode currentNode, ConstructorDropdownUICallback callback)
        {
            _dropdown.Clear();
            /*Dropdown.options.Add(new Dropdown.OptionData
            {
                text = "None"
            });*/

            if (variants == null)
            {
                return;
            }
            
            
            _txtSlotName.text = slotName;
            _dropdown.Clear();
            _buttons.Clear();
            for(var i = 0; i < variants.VariantsRefs.Length; i++)
            {
                var index = i+1;
                var variant = variants.VariantsRefs[i].asset;
                var btn = new Button();
                btn.RegisterCallback<ClickEvent>(_ => SelectCallback(index, btn, callback));
                btn.AddToClassList("constructor-variant-button");
                if (variant.Name.Equals(currentNode?.MountedItem?.Name))
                {
                    btn.AddToClassList("selected");
                }

                btn.text = variant.Name;
                _dropdown.Add(btn);
                _buttons.Add(btn);
            }
        }

        private void SelectCallback(int index, Button currentButton, ConstructorDropdownUICallback callback)
        {
            foreach (var button in _buttons)
            {
                button.RemoveFromClassList("selected");
            }
            currentButton.AddToClassList("selected");
            callback(index);
        }
    }
}