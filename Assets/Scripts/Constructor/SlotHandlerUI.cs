﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Constructor
{
    public class SlotHandlerUI : MonoBehaviour
    {
        private string _label = string.Empty;

        private bool _leftAlignment;

        private bool _isRequired;
        
        private readonly Color HiglightColor = new Color(1f, 0.88f, 0.58f);

        private readonly Color NormalColor = Color.white;

        private readonly Color MountedColor = new Color(0.26f, 1f, 0.67f);
        
        private readonly Color RequiredColor = new Color(1f, 0.44f, 0.41f);

        private bool _isSelected = false;
        
        private bool _isMounted = false;
        
        public TextMesh Text;

        public LineRenderer Line;

        public BoxCollider Collider;
        
        public UnityEvent OnSelected { get; } = new UnityEvent();

        public void Start()
        {
            ChangeColor();
        }

        public void OnMouseUp()
        {
            OnSelected.Invoke();
        }

        public void OnMouseEnter()
        {
            Text.color = HiglightColor;
        }
        

        public void OnMouseExit()
        {
            ChangeColor();
        }

        public void Setup(string label, Vector3 labelOffset, bool leftAlignment = false, bool required = false)
        {
            _leftAlignment = leftAlignment;
            _isRequired = required;
            _label = label;
            Text.transform.localPosition = labelOffset;
            SetText(label);
        }

        public void SetSelected(bool isSelected)
        {
            _isSelected = isSelected;
            if (_isSelected)
            {
                SetText($"[{_label}]");
            }
            else
            {
                SetText(_label);
            }
        }
        
        public void SetMounted(bool isMounted)
        {
            _isMounted = isMounted;
            ChangeColor();
        }

        private void ChangeColor()
        {
            Text.color = _isMounted ? MountedColor : _isRequired ? RequiredColor : NormalColor;
        }

        private void SetText(string text)
        {
            if (_leftAlignment)
            {
                Text.anchor = TextAnchor.UpperRight;
            }
            Text.text = text;
            DrawLine();
            FitCollider();
        }

        private void DrawLine()
        {
            var offset = new Vector3(_leftAlignment ? -0.1f : 0.1f, 0.5f, 0);
            var slotLabelPos = Text.gameObject.transform.localPosition - offset;

            var startPoint = Vector3.zero;
            var textStartPos = slotLabelPos;
            var textEndPos = CalculateTextEndPosition(slotLabelPos);
            
            Line.positionCount = 3;
            Line.SetPositions(new[]
            {
                startPoint,
                textStartPos,
                textEndPos
            });
        }

        private void FitCollider()
        {
            var bounds = Text.GetComponent<MeshRenderer>().bounds;
            var size = bounds.size;
            var center = transform.InverseTransformPoint(bounds.center);
            if (!_leftAlignment)
            {
                Collider.center = center;
                Collider.size = new Vector3(size.x, size.y, 0.1f);
            }
            else
            {
                Collider.center = center;
                Collider.size = new Vector3(size.x, size.y, 0.1f);
            }
        }

        private Vector3 CalculateTextEndPosition(Vector3 slotLabelPos)
        {
            var bounds = Text.GetComponent<MeshRenderer>().bounds;
            var offset = new Vector3(bounds.size.x + Text.characterSize*2, 0, 0);
            return _leftAlignment ? slotLabelPos - offset : slotLabelPos + offset;
        }
    }
}