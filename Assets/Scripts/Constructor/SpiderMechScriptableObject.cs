using UnityEngine;

namespace Constructor
{
    [CreateAssetMenu(fileName = "SpiderMech", menuName = "ScriptableObjects/SpiderMechScriptableObject", order = 1)]
    public class SpiderMechScriptableObject: ScriptableObject
    {
        public GameObject[] Legs;

        public GameObject[] Cockpits;
    
        public GameObject[] Backpacks;
    }
}
