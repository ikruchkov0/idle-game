using System;
using System.Collections.Generic;
using UnityEngine;

namespace Constructor
{
    [Serializable]
    public class MountPoint
    {
        public string Name;
        
        public string HierarchyPath;

        public ConstructablePartType Type;
        
        public LazyLoadReference<MountSlotVariantsSC> VariantsRef;

        public bool IsRequired;

        public MountSlotVariantsSC Variants => VariantsRef.asset;
    }

    [Serializable]
    public class ItemDescriptor
    {
        [TextArea]
        public string Feature;
    
        public int Firepower;
    
        public int Defense;
    }

    [CreateAssetMenu(fileName = "ConstructableItem", menuName = "ScriptableObjects/ConstructableItemSC", order = 1)]
    public class ConstructableItemSC: ScriptableObject
    {
        public string Name;
    
        public LazyLoadReference<GameObject> PrefabRef;
        
        public MountPoint[] MountPoints;

        public ItemDescriptor Descriptor = new ItemDescriptor();

        public ConstructablePartsMatcher PartMatcher;
        
        public GameObject Prefab => PrefabRef.asset;
        
        public void UpdateFromPrefab()
        {
            if (!PrefabRef.isSet)
            {
                return;
            }
            
            if (string.IsNullOrEmpty(Name))
            {
                Name = Prefab.name;
            }
            UpdateMountPoints();
        }
        
        private void UpdateMountPoints()
        {
            const string mountSign = "Mount_";
            var points = new List<MountPoint>();
            var items = Prefab.GetComponentsInChildren<Transform>();
            foreach (var item in items)
            {
                var go = item.gameObject;
                var slotName = go.name;
                if (!go.name.StartsWith(mountSign, StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }
                
                if (!PartMatcher.TryMatchPartSettings(slotName, out var  partTypeSettings))
                {
                    Debug.LogWarning($"Unknown type for slot name {slotName}");
                    continue;
                }
                
                var mountPoint = new MountPoint
                {
                    Name = partTypeSettings.DisplaySlotName,
                    Type = partTypeSettings.Type,
                    VariantsRef = partTypeSettings.VariantsRef,
                    IsRequired =  partTypeSettings.IsRequired,
                    HierarchyPath = RestorePath(go, Prefab)
                };

                points.Add(mountPoint);
            }
            
            MountPoints = points.ToArray();
        }
        
        private string RestorePath(GameObject gameObject, GameObject endParent)
        {
            var path = new List<string>();
            while (gameObject && gameObject != endParent)
            {
                path.Add(gameObject.name);
                gameObject = gameObject.transform.parent.gameObject;
            }

            path.Reverse();

            return string.Join("/", path);
        }
    }
}