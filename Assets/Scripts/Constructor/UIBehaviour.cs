﻿using System;
using System.Linq;
using Units;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;
using UnityEngine.UIElements;
using Vector3 = UnityEngine.Vector3;

namespace Constructor
{
    public class UIBehaviour : MonoBehaviour
    {
        private MountNode _startNode;
        private MountNode _currentNode;
        private DropdownUI _dropdown;
        private Button _btnRotateLeft;
        private Button _btnRotateRight;
        private Button _btnAssemble;
        private Button _btnDeploy;
        private VisualElement _mainPanel;
        private TextField _txtUnitName;

        private SummaryUI _summary;
        
        public GameObject ShowPoint;

        public MountPoint PlatformMountPoint;
        
        public SlotHandlerUI platformSlotHandlerUI;
        
        [RequiredMember]
        public UIDocument UIDocument;

        public SlotHandlerFactory SlotHandlerFactory;

        [RequiredMember]
        public UnitRenderer UnitRenderer;
        
        public void Start()
        {
            InitUI();
            
            platformSlotHandlerUI.Setup("Platform", Vector3.zero);
            _startNode = new MountNode(PlatformMountPoint, ShowPoint, platformSlotHandlerUI);
            
            _startNode.OnSelected.AddListener(OnNodeSelected);
            _summary.UpdateCurrent(null);
            _summary.UpdateSummary(_startNode);
            
            OnNodeSelected(_startNode);
            UpdateAssembleButtonState();
        }
        
        private UnitSC BuildUnit()
        {
            var unit = ScriptableObject.CreateInstance<UnitSC>();
            unit.Name = "Name Your Unit";
            unit.Body = _startNode.ToBodyPart();
            return unit;
        }
        
        private void InitUI()
        {
            var root = UIDocument.rootVisualElement;
            _mainPanel = root.Q<VisualElement>("MainPanel");
            _txtUnitName = root.Q<TextField>("txtUnitName");
            _dropdown = new DropdownUI(root);
            _summary = new SummaryUI(root);
            _btnRotateLeft = root.Q<Button>("btnRotateLeft");
            _btnRotateRight = root.Q<Button>("btnRotateRight");
            _btnAssemble = root.Q<Button>("btnAssemble");
            _btnDeploy = root.Q<Button>("btnDeploy");

            _btnRotateLeft.RegisterCallback<ClickEvent>(_ =>
            {
                var euler = ShowPoint.transform.eulerAngles;
                euler.y -= 10;
                ShowPoint.transform.eulerAngles = euler;
            });
            
            _btnRotateRight.RegisterCallback<ClickEvent>(_ =>
            {
                var euler = ShowPoint.transform.eulerAngles;
                euler.y += 10;
                ShowPoint.transform.eulerAngles = euler;
            });
            
            _btnAssemble.RegisterCallback<ClickEvent>(_ =>
            {
                UnitRenderer.Unit = BuildUnit();
                _startNode.Unmount();
                UnitRenderer.RenderUnit();
                _mainPanel.RemoveFromClassList("construct-state");
                _mainPanel.AddToClassList("rename-state");
                _txtUnitName.value = UnitRenderer.Unit.Name;
                _btnAssemble.AddToClassList("hidden");
                _btnDeploy.RemoveFromClassList("hidden");
            });
            
            _btnDeploy.RegisterCallback<ClickEvent>(_ =>
            {
                UnitRenderer.Unit.Name = _txtUnitName.value;

                UnitManager.SetConstructedUnit(UnitRenderer.Unit);
                SceneManager.LoadScene("Scenes/Main");
            });
        }

        private void OnNodeSelected(MountNode node)
        {
            _currentNode?.Deselect();
            _dropdown.SetupSlot(node.SlotName, node.Variants, node, i => SlotItemSelected(i, node));
            _currentNode = node;
            _currentNode.Select();
            _summary.UpdateCurrent(_currentNode.MountedItem);
        }

        private void SlotItemSelected(int index, MountNode mountNode)
        {
            if (mountNode.IsMounted)
            {
                mountNode.Unmount();
            }

            if (index == 0)
            {
                return;
            }

            index--;

            var variant = mountNode.Variants.VariantsRefs[index].asset;

            var item = Instantiate(variant.Prefab, mountNode.MountPoint.transform, false);
            var animator = item.GetComponent<Animator>();
            if (animator != null)
            {
                animator.enabled = false;
            }

            mountNode.Mount(variant, item);

            foreach (var mountPoint in variant.MountPoints)
            {
                if (string.IsNullOrEmpty(mountPoint.HierarchyPath))
                {
                    Debug.LogError($"Slot {mountPoint.Name} path is empty");
                    continue;
                }
                var slot = item.transform.Find(mountPoint.HierarchyPath);
                if (slot == null)
                {
                    Debug.LogError($"Slot with path {mountPoint.HierarchyPath} is not found");
                    continue;
                }

                if (mountPoint.Variants == null || mountPoint.Variants.VariantsRefs.Length == 0)
                {
                    continue;
                }

                var newMountNode = new MountNode(mountPoint, slot.gameObject, SlotHandlerFactory.Create);
                newMountNode.OnSelected.AddListener(OnNodeSelected);
                mountNode.BindChildNode(newMountNode);
            }

            _summary.UpdateCurrent(variant);
            _summary.UpdateSummary(_startNode);

            UpdateAssembleButtonState();
        }

        private void UpdateAssembleButtonState()
        {
            if (_startNode.IsValid())
            {
                _btnAssemble.RemoveFromClassList("hidden");
            }
            else
            {
                _btnAssemble.AddToClassList("hidden");
            }
        }
    }
}