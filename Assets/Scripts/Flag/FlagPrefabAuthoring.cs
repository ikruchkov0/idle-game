﻿using Hybrid.Disable;
using Hybrid.Positioning;
using Prefabs;
using Unity.Entities;
using UnityEngine;

namespace Flag
{
    public class FlagPrefabAuthoring : TilePrefabAuthoring<FlagComponent>
    {
        private Entity? _characterEntity;

        public Entity? CharacterEntity
        {
            get => _characterEntity;
            set
            {
                _characterEntity = value;
                UpdateCharacterEntity();
            }
        }

        public FlagPrefabAuthoring()
        {
            staticOffset = new Vector3(0f, 0.15f, 0f);
        }

        protected override void ApplyComponents(in Entity entity, ref EntityManager entityManager)
        {
            base.ApplyComponents(entity, ref entityManager);
            entityManager.AddComponentData(entity, new HybridPositioningComponent());
            HybridDisabledComponent.Apply(ref entityManager, entity);

            UpdateCharacterEntity();
        }

        private void UpdateCharacterEntity()
        {
            if (Entity.HasValue && EntityManager.HasValue && _characterEntity.HasValue)
            {
                EntityManager.Value.AddComponentData(_characterEntity.Value, new TargetFlagComponent(Entity.Value));
            }
        }
    }
}