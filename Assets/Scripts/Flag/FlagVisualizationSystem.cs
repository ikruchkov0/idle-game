﻿using Hybrid.Disable;
using Pathfinding;
using Tiles;
using Unity.Entities;
using WaypointSign;

namespace Flag
{
    [UpdateBefore(typeof(PathfindingSystem))]
    public class FlagVisualizationSystem: SystemBase
    {
        EndSimulationEntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            _commandBufferSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            base.OnCreate();
        }
        
        protected override void OnUpdate()
        {
            var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();
            
            Entities
                .ForEach((in Entity charEntity, 
                    in int entityInQueryIndex,
                    in DynamicBuffer<WaypointElement> waypoints,
                    in TargetFlagComponent targetFlag) =>
                {
                    if (waypoints.Length <= 0)
                    {
                        HybridDisabledComponent.Apply(ref ecb, entityInQueryIndex, targetFlag.Flag);
                        return;
                    }
                    var lastWaypoint = waypoints[waypoints.Length - 1];
                    ecb.AddComponent(entityInQueryIndex, targetFlag.Flag, new TilePositionComponent(lastWaypoint.Pos));
                    HybridEnabledComponent.Apply(ref ecb, entityInQueryIndex, targetFlag.Flag);
                })
                .ScheduleParallel();
            
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}