﻿using Actions;
using Unity.Entities;

namespace Flag
{
    public class RemoveFlagSystem: SystemBase
    {
        EndSimulationEntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            _commandBufferSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            base.OnCreate();
        }
        
        protected override void OnUpdate()
        {
            var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();
            
            Entities
                .WithAll<IdleComponent>()
                .ForEach((in Entity entity, 
                    in int entityInQueryIndex,
                    in TargetFlagComponent targetFlag) =>
                {
                    if (HasComponent<Disabled>(targetFlag.Flag))
                    {
                        return;
                    }
                    
                    ecb.AddComponent<Disabled>(entityInQueryIndex, targetFlag.Flag);
                })
                .ScheduleParallel();
            
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}