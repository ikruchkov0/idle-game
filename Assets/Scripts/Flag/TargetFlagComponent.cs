﻿using Unity.Entities;

namespace Flag
{
    public readonly struct TargetFlagComponent: IComponentData
    {
        public readonly Entity Flag;

        public TargetFlagComponent(in Entity flag)
        {
            Flag = flag;
        }
    }
}