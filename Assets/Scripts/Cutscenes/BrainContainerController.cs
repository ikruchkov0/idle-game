using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cutscenes
{
    public class BrainContainerController : MonoBehaviour
    {
        public enum LightType
        {
            Top, Bottom, Left, Right
        }
        
        public class LightDescriptor
        {
            public LightDescriptor(Light light, float changeSpeed)
            {
                Light = light;
                InitialIntensity = light.intensity;
                light.intensity = 0;
                ChangeSpeed = changeSpeed;
            }

            public Light Light { get; }
            
            public float InitialIntensity { get; }
            
            public float ChangeSpeed { get; }
        }

        public ParticleSystem FrontBubbles;
        
        public ParticleSystem BackBubbles;
       
        public Light TopLight;
    
        public Light BottomLight;
        
        public Light RightLight;
        
        public Light LeftLight;

        public GameObject Brain;

        private IReadOnlyDictionary<LightType, LightDescriptor> _descriptors;
        
        void Start()
        {
            const float speed = 0.5f;
            _descriptors = new Dictionary<LightType, LightDescriptor>
            {
                [LightType.Top] = new LightDescriptor(TopLight, speed),
                [LightType.Bottom] = new LightDescriptor(BottomLight, speed),
                [LightType.Left] = new LightDescriptor(LeftLight, speed),
                [LightType.Right] = new LightDescriptor(RightLight, speed),
            };
            FrontBubbles.Stop();
            BackBubbles.Stop();
        }

        public void StartBubbles()
        {
            FrontBubbles.Play();
            BackBubbles.Play();
        }
        
        public void Lightup(LightType type)
        {
            if (!_descriptors.TryGetValue(type, out var descriptor))
            {
                throw new Exception($"Light not found {type}");
            }

            StartCoroutine(AnimateLight(descriptor));
        }
        
        public IEnumerator AnimateLight(LightDescriptor descriptor)
        {
            var intensity = descriptor.Light.intensity;
            var delta = descriptor.InitialIntensity - intensity;

            while (Math.Abs(delta) > descriptor.ChangeSpeed)
            {
                yield return null;
                
                intensity = descriptor.Light.intensity;
                delta = descriptor.InitialIntensity - intensity;
                
                intensity += descriptor.ChangeSpeed * delta * Time.deltaTime;
                descriptor.Light.intensity = intensity;
            }
        }
    }
}
