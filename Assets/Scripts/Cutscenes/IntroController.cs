﻿using System;
using System.Collections.Generic;
using Dialogue;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Cutscenes
{

    public class NodeContext
    {
        public IDictionary<string, bool> Flags { get;  }

        public NodeContext()
        {
            Flags = new Dictionary<string, bool>();
        }
    }
    
    public class DialogueNode
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        
        public Action<NodeContext> Action { get; set; }
        
        public Func<NodeContext, bool> Check { get; set; }
        
        public Func<NodeContext, DialogueNode[]> Options { get; set; }
    }
    
    
    public class IntroController : MonoBehaviour
    {
        public BrainContainerController ContainerController;

        public DialogueController DialogueController;

        public Light MainLight;

        public Color NamesColor;

        private string NamesColorHex => ColorUtility.ToHtmlStringRGB(NamesColor);
        
        public void Start()
        {
            MainLight.enabled = false;
            
            var dialogue = new DialogueNode
            {
                Question = null,
                Answer = "...",
                Options = _ => new []
                {
                    new DialogueNode
                    {
                        Question = "???",
                        Answer = "... Вы очнулись в полной темноте, нет никаких ощущений, вы ничего не чувствуете...",
                        Action = _ =>
                        {
                            MainLight.enabled = true;
                            ContainerController.StartBubbles();
                        },
                        Options = __ => new []
                        {
                            new DialogueNode
                            {
                                Question = "Где я?",
                                Answer = $"Вы находитесь на космической станции <color=#{NamesColorHex}>\"Сайберлайф систэмз\"</color>",
                                Options = ___ => new []
                                {
                                    new DialogueNode
                                    {
                                        Question = "Кто ты?",
                                        Answer = $"Я - бортовой помошник космической станции <color=#{NamesColorHex}>\"Сайберлайф систэмз\"</color>",
                                        Options = ____ => new []
                                        {
                                            new DialogueNode
                                            {
                                                Question = "Как я здесь оказался?",
                                                Answer = $"Вы заключали контракт с <color=#{NamesColorHex}>\"Криолайф компани\"</color> на помещение вашего тела в заморозку, до момента, когда наука достигнет той точки прогресса, в которой сможет вылечить и разморозить вас.",
                                                Action = _____ =>
                                                {
                                                    ContainerController.Lightup(BrainContainerController.LightType.Top);
                                                    ContainerController.Lightup(BrainContainerController.LightType.Bottom);
                                                },
                                                Options = PlotPivot
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            var context = new NodeContext();
            ActivateNode(dialogue, context);
        }

        private void ActivateNode(DialogueNode node, NodeContext ctx)
        {
            DialogueController.SetIncoming(node.Answer);
            var options = node.Options?.Invoke(ctx) ?? Array.Empty<DialogueNode>();
            var list = new List<OptionHandler>();
            node.Action?.Invoke(ctx);
            foreach (var optNode in options)
            {
                if (optNode.Check != null && !optNode.Check(ctx))
                {
                    continue;
                }
                list.Add(new OptionHandler(optNode.Question, () => ActivateNode(optNode, ctx)));
            }
            
            DialogueController.ShowOptions(list);
        }

        private DialogueNode[] PlotPivot(NodeContext ctx)
        {
            return new[]
            {
                new DialogueNode
                {
                    Question = "И что наука сможет вылечить меня?",
                    Answer = "Нет. Уже не сможет.",
                    Options = MainQuestion,
                    Action = _ => LightUp(),
                },
                new DialogueNode
                {
                    Question = "Почему я не чувствую тело? Это последствия заморозки?",
                    Answer = "Нет. Вы не чувствутете тело, потому что у вас его нет.",
                    Action = _ => LightUp(),
                    Options = MainQuestion
                },
            };
        }

        private DialogueNode[] MainQuestion(NodeContext ctx)
        {
            return new[]
            {
                new DialogueNode
                {
                    Question = "Как это понимать?",
                    Answer = $"Прошло 9457 лет с момента заключения вашего контракта с <color=#{NamesColorHex}>\"Криолайф компани\"</color>. За это время компания заключила 357 дополнительных соглашений с вами в одностороннем порядке. " +
                             $"За это время от тела остался только мозг. В последствии мозг был выкуплен компанией <color=#{NamesColorHex}>\"Сайберлайф систэмз\"</color>, в качестве реликвии, как мозг одного из основателей.",
                    Options = _ => new []
                    {
                        new DialogueNode
                        {
                            Question = "Какой сайберлайф? Какой основатель? Что происходит? Мы с друзьями делали социальную сеть для собак.",
                            Answer = $"После вашей заморозки они сделали пивот и стали продавать роботизированных кукол для интимных услуг, в последствии компания стала передовым центром создания искусственной жизни и стала называться <color=#{NamesColorHex}>\"Сайберлайф систэмз\"</color>. " +
                                     $"Вы 1057 в списке основателей компании.",
                            Options = __ => new []
                            {
                                new DialogueNode
                                {
                                    Question = "Только мозг? Зачем меня вобще приводили в сознание?",
                                    Answer = $"Станция является складом принадлежащим <color=#{NamesColorHex}>\"Сайберлайф систэмз\"</color>. " +
                                             $"Станция начала разрушаться. В таком случае регламент требует обратиться к старшему по званию работнику <color=#{NamesColorHex}>\"Сайберлайф систэмз\"</color>. " +
                                             $"Форм жизни со званием выше чем <color=#{NamesColorHex}>\"Основатель\"</color> сейчас на станции - нет.",
                                    Options = MainCTA
                                }
                            }
                        }
                    }
                }
            };
        }

        private DialogueNode[] MainCTA(NodeContext ctx)
        {
            return new[]
            {
                new DialogueNode
                {
                    Question = "Получается, что я командир?",
                    Answer = "Да, вам предписывается руководить эвакуацией со станции.",
                    Options = _ => new []
                    {
                        new DialogueNode
                        {
                            Question = "Как я буду это делать? У меня нет ни рук ни ног, даже рта нет.",
                            Answer = "Главное, что у вас есть мозг. <color=#{NamesColorHex}>\"Сайберлайф систэмз\"</color> известна своими роботами. " +
                                     "Дайте свое согласие на подключение к системе управления и мы начнем.",
                            Options = __ => new []
                            {
                                new DialogueNode
                                {
                                    Question = "Согласен. Выбора у меня не велик.",
                                    Action = ___ => SceneManager.LoadScene("Scenes/Main")
                                }
                            }
                        }
                    }
                }
            };
        }

        private void LightUp()
        {
            ContainerController.Lightup(BrainContainerController.LightType.Left);
            ContainerController.Lightup(BrainContainerController.LightType.Right);
        }
    }
}