﻿using Unity.Entities;
using Unity.Physics;

namespace UserInput
{
    public readonly struct RayCastInputComponent : IComponentData
    {
        public readonly RaycastInput Ray;

        public RayCastInputComponent(RaycastInput ray)
        {
            Ray = ray;
        }
    }
}