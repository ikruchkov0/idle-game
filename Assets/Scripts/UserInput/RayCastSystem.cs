﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics.Systems;
using RaycastHit = Unity.Physics.RaycastHit;

namespace UserInput
{
    [UpdateBefore(typeof(TouchCleanupSystem))]
    public class RayCastSystem: SystemBase
    {
        private BuildPhysicsWorld _physicsSystem;
        EndSimulationEntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            _physicsSystem = World.GetExistingSystem<BuildPhysicsWorld>();
            _commandBufferSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();
            var collisionWorld = _physicsSystem.PhysicsWorld.CollisionWorld;

            var dependency = JobHandle.CombineDependencies(_physicsSystem.GetOutputDependency(), Dependency);
            
            var jobHandle = Entities
                .WithReadOnly(collisionWorld)
                .ForEach((in Entity entity, in int entityInQueryIndex, in RayCastInputComponent touchComponent) =>
                {
                    var rayInput = touchComponent.Ray;
                    
                    var rayCastHits = new NativeList<RaycastHit>(5, Allocator.Temp);
                    
                    if (collisionWorld.CastRay(rayInput, ref rayCastHits))
                    {
                        for (var i = 0; i < rayCastHits.Length; i++)
                        {
                            var hit = rayCastHits[i];
                            var hitEntity = hit.Entity;
                            if (HasComponent<TouchableComponent>(hitEntity))
                            {
                                ecb.AddComponent(entityInQueryIndex, hitEntity, new TouchedComponent());
                                break;
                            }
                        }
                    }

                    rayCastHits.Dispose(); //TODO: rethink it and prevent possible memory leak

                    ecb.DestroyEntity(entityInQueryIndex, entity);
                })
                .Schedule(dependency);
            
            _commandBufferSystem.AddJobHandleForProducer(jobHandle);
        }
    }
}