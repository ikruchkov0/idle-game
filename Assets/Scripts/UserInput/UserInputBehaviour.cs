﻿using Unity.Entities;
using Unity.Physics;
using UnityEngine;
using UnityEngine.Serialization;
using TouchPhase = UnityEngine.TouchPhase;

namespace UserInput
{
    public class UserInputBehaviour : MonoBehaviour
    
    {
        private const int RaycastDistance = 1000;
        [FormerlySerializedAs("Cam")] public Camera cam;
        private bool _isCamNull;

        private EntityManager entityManager => World.DefaultGameObjectInjectionWorld.EntityManager;

        public void Start()
        {
            _isCamNull = cam == null;
        }
        
        public void LateUpdate()
        {
            if (_isCamNull) return;
            if (HasNoAction) return;

            var pos = GetActionPos();

            var screenPointToRay = cam.ScreenPointToRay(pos);
            var rayInput = new RaycastInput
            {
                Start = screenPointToRay.origin,
                End = screenPointToRay.GetPoint(RaycastDistance),
                Filter = CollisionFilter.Default
            };

            var touchEntity = entityManager.CreateEntity(typeof(RayCastInputComponent));
            entityManager.AddComponentData(touchEntity, new RayCastInputComponent(rayInput));
        }

        private bool HasNoAction => Input.touchCount <= 0 && !Input.GetMouseButtonDown(0);

        private Vector3 GetActionPos()
        {
            if (Input.GetMouseButtonDown(0))
            {
                return Input.mousePosition;
            }
        
            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase.Equals(TouchPhase.Began))
                {
                    return Input.GetTouch(i).position;
                }
            }
            
            return Vector3.zero;
        }
    
    }
}
