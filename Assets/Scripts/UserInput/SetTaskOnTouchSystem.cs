﻿using System;
using Character;
using Hybrid.Navigation;
using Tiles;
using Unity.Collections;
using Unity.Entities;

namespace UserInput
{
    [UpdateBefore(typeof(TouchCleanupSystem))]
    public class SetTaskOnTouchSystem: SystemBase
    {
        EndSimulationEntityCommandBufferSystem _commandBufferSystem;
        
        protected override void OnCreate()
        {
            _commandBufferSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();
            
            var charactersArray = EntityManager
                .CreateEntityQuery(typeof(CharacterComponent))
                .ToEntityArray(Allocator.TempJob);
            
            if (charactersArray.Length == 0)
            {
                throw new Exception("No characters found");
            }
            
            Entities
                .WithReadOnly(charactersArray)
                .WithAll<TouchedComponent>()
                .ForEach((in Entity entity, in int entityInQueryIndex, in TilePositionComponent tilePosition) =>
                {
                    var character = charactersArray[0];

                    /*if (HasComponent<CurrentAction>(character))
                    {
                        Debug.LogError("Is busy");
                        return;
                    }*/
                    
                    //ecb.AddComponent(entityInQueryIndex, character, new PathfindingTaskComponent(tilePosition.Pos));
                    ecb.AddComponent(entityInQueryIndex, character, new NavAgentTargetComponent(tilePosition.Pos));
                })
                .WithDisposeOnCompletion(charactersArray)
                .Schedule();
            
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}