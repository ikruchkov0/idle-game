﻿using Unity.Entities;

namespace UserInput
{
    public class TouchCleanupSystem: SystemBase
    {
        EndSimulationEntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            _commandBufferSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();
            
            Entities
                .WithAll<TouchedComponent>()
                .ForEach((in Entity entity, in int entityInQueryIndex) =>
                {
                    ecb.RemoveComponent<TouchedComponent>(entityInQueryIndex, entity);
                })
                .Schedule();
            
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}