﻿using Actions;
using Animation;
using Character;
using Unity.Entities;

namespace CharacterIdle
{
    [UpdateBefore(typeof(ActionSystem))]
    public class CharacterIdleSystem: SystemBase
    {
        EndSimulationEntityCommandBufferSystem _commandBufferSystem;
        
        protected override void OnCreate()
        {
            _commandBufferSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();
            
            Entities
                .WithAll<CharacterComponent, IdleComponent>()    
                .ForEach((
                    in int entityInQueryIndex,
                    in Entity entity) =>
                {
                    if (HasComponent<AnimationComponent>(entity))
                    {
                        var currentAnimation = GetComponent<AnimationComponent>(entity);
                        if (currentAnimation.Animation == Animations.Idle)
                        {
                            return;
                        }
                    }
                    
                    ecb.AddComponent(entityInQueryIndex, entity, new AnimationComponent(Animations.Idle, 1f));
                })
                .Schedule();
            
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}