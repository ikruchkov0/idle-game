﻿using Hybrid.Positioning;
using Unity.Entities;

namespace Prefabs
{
    public class RackPrefabAuthoring : TilePrefabAuthoring<RackComponent>
    {
        public RackPrefabAuthoring()
        {
            IsWalkable = false;
        }
        
        protected override void ApplyComponents(in Entity entity, ref EntityManager entityManager)
        {
            base.ApplyComponents(entity, ref entityManager);
            entityManager.AddComponentData(entity, new HybridPositioningComponent());
        }
    }
}