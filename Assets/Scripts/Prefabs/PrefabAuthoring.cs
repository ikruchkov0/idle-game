﻿using Unity.Entities;
using UnityEngine;

namespace Prefabs
{
    public abstract class PrefabAuthoring<T> : MonoBehaviour, IConvertGameObjectToEntity
        where T: struct, IComponentData
    {
        protected Entity? Entity;
        protected EntityManager? EntityManager;
        
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            Entity = entity;
            EntityManager = dstManager;
            dstManager.AddComponentData(entity, new PrefabComponent());
            dstManager.AddComponentData(entity, new T());
            ApplyComponents(entity, ref dstManager);
        }

        protected abstract void ApplyComponents(in Entity entity, ref EntityManager entityManager);
    }
}