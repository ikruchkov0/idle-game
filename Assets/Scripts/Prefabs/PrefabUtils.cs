﻿using Unity.Collections;
using Unity.Entities;

namespace Prefabs
{
    public static class PrefabUtils
    {
        public static bool TryGetPrefabEntity<T>(ref EntityManager entityManager, out Entity entity)
        where T: struct, IComponentData
        {
            using (var prefabs = GetPrefabs<T>(ref entityManager))
            {
                if (prefabs.Length == 0)
                {
                    entity = default;
                    return false;
                }
                
                entity = prefabs[0];
                return true;
            }
        }
        
        public static NativeArray<Entity> GetPrefabs<T>(ref EntityManager entityManager)
        where T: struct, IComponentData
        {
            return entityManager
                .CreateEntityQuery(typeof(T), typeof(PrefabComponent))
                .ToEntityArray(Allocator.TempJob);
        }
    }
}