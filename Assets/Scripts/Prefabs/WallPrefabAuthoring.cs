﻿using Hybrid.Positioning;
using Unity.Entities;
using UnityEngine;

namespace Prefabs
{
    public class WallPrefabAuthoring : TilePrefabAuthoring<WallComponent>
    {
        public WallPrefabAuthoring()
        {
            staticOffset = new Vector3(0f, 0.65f, 0f);
        }

        protected override void ApplyComponents(in Entity entity, ref EntityManager entityManager)
        {
            base.ApplyComponents(entity, ref entityManager);
            entityManager.AddComponentData(entity, new HybridPositioningComponent());
        }
    }
}