﻿using Hybrid.Positioning;
using Tiles;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Serialization;

namespace Prefabs
{
    public class GroundPrefabAuthoring : TilePrefabAuthoring<GroundComponent>
    {
        public GroundPrefabAuthoring()
        {
            staticOffset = new Vector3(0f, 0.15f, 0f);
        }

        protected override void ApplyComponents(in Entity entity, ref EntityManager entityManager)
        {
            base.ApplyComponents(entity, ref entityManager);
            entityManager.AddComponentData(entity, new HybridPositioningComponent());
        }
    }
}