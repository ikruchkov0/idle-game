﻿using Tiles;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Serialization;
using UserInput;

namespace Prefabs
{
    public class TilePrefabAuthoring<T> : PrefabAuthoring<T> 
        where T: struct, IComponentData
    {
        [FormerlySerializedAs("Static Offset")]
        [SerializeField]
        public Vector3 staticOffset = new Vector3(0f, 0.15f, 0f);

        public int I = -1;

        public int J = -1;

        public bool IsWalkable = false;

        public bool IsTouchable = false;
        
        protected override void ApplyComponents(in Entity entity, ref EntityManager entityManager)
        {
            var offset = new TileStaticOffsetComponent(staticOffset.x, staticOffset.y, staticOffset.z);
            entityManager.AddComponentData(entity, offset);
            if (I != -1 && J != -1)
            {
                entityManager.AddComponentData(entity, new TilePositionComponent(I, J));
            }
            
            if (IsWalkable)
            {
                entityManager.AddComponentData(entity, new WalkableComponent());
            }
            else
            {
                entityManager.AddComponentData(entity, new NonWalkableComponent());
            }

            if (IsTouchable)
            {
                entityManager.AddComponentData(entity, new TouchableComponent());
            }
        }
    }
}