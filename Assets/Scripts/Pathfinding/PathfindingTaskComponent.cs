﻿using Unity.Entities;
using Unity.Mathematics;

namespace Pathfinding
{
    public readonly struct PathfindingTaskComponent: IComponentData
    {
        public readonly int2 Pos;

        public int I => Pos.x;

        public int J => Pos.y;
        
        public PathfindingTaskComponent(in int2 pos)
        {
            Pos = pos;
        }
    }
}