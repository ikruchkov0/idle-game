﻿using System;
using Tiles;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;

namespace Pathfinding
{
    public struct TilesMap: IDisposable
    {
        private readonly int _fieldSize;
        private NativeArray<TileType> _tilesMap;

        public TilesMap(
            in Allocator allocator,
            in int fieldSize)
        {
            _fieldSize = fieldSize;
            _tilesMap = new NativeArray<TileType>(fieldSize * fieldSize, allocator);
        }

        public int Size => _fieldSize;
        
        public TileType this[int2 point] => _tilesMap[GetKey(point)];

        [BurstCompile]
        public void PopulateFromObstacle(in NativeArray<TilePositionComponent> obstacles)
        {
            for (int i = 0; i < obstacles.Length; i++)
            {
                var obstacle = obstacles[i];
                _tilesMap[GetKey(obstacle.Pos)] = TileType.Obstacle;
            }
        }

        public void Dispose()
        {
            if (_tilesMap.IsCreated)
            {
                _tilesMap.Dispose();
            }
        }
        
        private int GetKey(int2 point)
        {
            return GetKey(point.x, point.y);
        }

        private int GetKey(int i, int j)
        {
            return j * _fieldSize + i;
        }
    }
}