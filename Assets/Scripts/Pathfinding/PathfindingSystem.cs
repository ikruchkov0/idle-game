﻿using Actions;
using Factory;
using Hybrid.TextMesh;
using Tiles;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Pathfinding
{
    
    public class PathfindingSystem: SystemBase
    {
        private EndSimulationEntityCommandBufferSystem _commandBufferSystem;
        
        protected override void OnCreate()
        {
            _commandBufferSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

            base.OnCreate();
        }
        
        protected override void OnUpdate()
        {
            var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();
            
            var size = WorldFactorySystem.BlocksPerSize;
            var tileMap = new TilesMap(Allocator.TempJob, size);
            
            var actionDelegates = new NativeHashMap<uint, ActionDelegates>(2, Allocator.TempJob);
            
            ActionDelegatesFactory.PopulateDelegates(ref actionDelegates);
            
            using (var obstacles = EntityManager
                .CreateEntityQuery(
                    typeof(NonWalkableComponent), 
                    typeof(TilePositionComponent))
                .ToComponentDataArray<TilePositionComponent>(Allocator.TempJob))
            {
                tileMap.PopulateFromObstacle(in obstacles);
            }

            var textMeshes = EntityManager
                .CreateEntityQuery(typeof(TextMeshComponent))
                .ToEntityArray(Allocator.TempJob);
            
            var textMeshesPositions = new NativeArray<TilePositionComponent>(textMeshes.Length, Allocator.TempJob);
            for (int i = 0; i < textMeshes.Length; i++)
            {
                textMeshesPositions[i] = GetComponent<TilePositionComponent>(textMeshes[i]);
            }
            
            Entities
                .ForEach((
                    ref DynamicBuffer<WAction> actions,
                    in int entityInQueryIndex,
                    in Entity entity,
                    in TilePositionComponent startPositionComponent, 
                    in PathfindingTaskComponent pathfindingTask
                    ) =>
                {
                    actions.Clear();
                    
                    if (tileMap[pathfindingTask.Pos] == TileType.Obstacle)
                    {
                        ecb.RemoveComponent<PathfindingTaskComponent>(entityInQueryIndex, entity);
                        return;
                    }
                    
                    Debug.Log($"Start pathfinding [{pathfindingTask.I},{pathfindingTask.J}]");
                    
                    var waypoints = new NativeList<int2>(Allocator.Temp);
                    var tilesEnumerator = new TilesEnumerator(tileMap);
                    var pathfinder = new Pathfinder(tilesEnumerator);
                        
                    int2 start = new int2();
                    if (HasComponent<CurrentAction>(entity))
                    {
                        var currentAction = GetComponent<CurrentAction>(entity);
                        if (currentAction.Current.ActionType == WActionType.Waypoint)
                        {
                            start = currentAction.Current.Target;
                        }
                    }
                    else
                    {
                        start = startPositionComponent.Pos;
                    }
                    
                    var target = pathfindingTask.Pos;
                    
                    var stepsMap = new StepsMap(Allocator.Temp, size);
                    
                    if (pathfinder.FindPath(ref waypoints, ref stepsMap, start, target))
                    {
                        for (var i = 0; i < waypoints.Length; i++)
                        {
                            var headingAction = WAction.CreateHeading(actionDelegates, actions.Length, waypoints[i]);
                            var waypointAction = WAction.CreateWaypoint(actionDelegates, actions.Length, waypoints[i]);
                            actions.Add(headingAction);
                            actions.Add(waypointAction);
                        }                       
                    }

                    VisualizeSteps(entityInQueryIndex, ref ecb, stepsMap, textMeshes, textMeshesPositions);
                    
                    stepsMap.Dispose();
                    
                    ecb.RemoveComponent<PathfindingTaskComponent>(entityInQueryIndex, entity);
                    
                    Debug.Log($"End pathfinding [{pathfindingTask.I},{pathfindingTask.J}]");
                })
                .WithReadOnly(actionDelegates)
                .WithDisposeOnCompletion(textMeshesPositions)
                .WithDisposeOnCompletion(textMeshes)
                .WithDisposeOnCompletion(actionDelegates)
                .WithDisposeOnCompletion(tileMap)
                .ScheduleParallel();
            
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void VisualizeSteps(
            int entityInQueryIndex,
            ref EntityCommandBuffer.ParallelWriter ecb,
            in StepsMap stepsMap,
            in NativeArray<Entity> textMeshes, 
            in NativeArray<TilePositionComponent> textMeshesPositions)
        {
            if (textMeshes.Length == 0)
            {
                return;
            }
            var size = stepsMap.Size;
            for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
            {
                Entity textMesh;
                for (int m = 0; m < textMeshes.Length; m++)
                {
                    textMesh = textMeshes[m];
                    var pos = textMeshesPositions[m];
                    if (pos.I != i || pos.J != j)
                    {
                        continue;
                    }

                    var text = new FixedString64();
                    var color = new Color(1f, 0, 0, 0.5f);
                    if (stepsMap.TryGet(new int2(i, j), out var stepsNumber))
                    {
                        text.Append(stepsNumber);
                        color = new Color(0f, 0f, 0f, 0.5f);
                    }
                    else
                    {
                        FixedString32 starString = "*";
                        text.Append(starString);
                    }

                    var tmc = new TextMeshComponent(text, color);

                    ecb.AddComponent(entityInQueryIndex, textMesh, tmc);
                }
            }
        }
    }
}