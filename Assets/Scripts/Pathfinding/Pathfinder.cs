﻿using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace Pathfinding
{
    public readonly struct Pathfinder
    {
        private readonly TilesEnumerator _tilesEnumerator;

        public Pathfinder(in TilesEnumerator tilesEnumerator)
        {
            _tilesEnumerator = tilesEnumerator;
        }

        public bool FindPath(
            ref NativeList<int2> waypoints,
            ref StepsMap stepsMap,
            in int2 start,
            in int2 target)
        {
            var startStep = 0;
            
            stepsMap.MarkIfLessStepNumber(start, startStep);

            var size = stepsMap.Size;

            for (int step = startStep; step < size * size; step++)
            {
                for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                {
                    var point = new int2(i, j);

                    if (!stepsMap.TryGet(point, out var currentStep)
                        || currentStep != step)
                    {
                        continue;
                    }
                    
                    _tilesEnumerator.Mark(ref stepsMap, step, point);
                }
            }
            
            waypoints.Clear();
            
            var result = false;
            
            if (stepsMap.TryGet(target, out var finalSteps))
            {
                if (BuildPath(ref waypoints, ref stepsMap, finalSteps, target))
                {
                    result = true;
                    ReverseWaypoints(ref waypoints);
                }
                else
                {
                    Debug.LogError("HierarchyPath impossible to build");
                }
            }
            else
            {
                Debug.LogError("Final point is not found");
            }
            
            if (!result)
            {
                waypoints.Clear();
            }

            return result;
        }

        private void ReverseWaypoints(ref NativeList<int2> waypoints)
        {
            int start;
            int end;
            for (start = 0, end = waypoints.Length - 1; start <= end; start++, end--)
            {
                var temp = waypoints[start];
                waypoints[start] = waypoints[end];
                waypoints[end] = temp;
            }
        }
        
        private bool BuildPath(
            ref NativeList<int2> waypoints,
            ref StepsMap stepsMap,
            in int stepNumber,
            in int2 point)
        {
            if (stepNumber == 0)
            {
                return true;
            }
            
            waypoints.Add(point);

            for (int deltaI = -1; deltaI <= 1; deltaI++)
            {
                for (int deltaJ = -1; deltaJ <= 1; deltaJ++)
                {
                    var nextPoint = new int2(point.x + deltaI, point.y + deltaJ);

                    if (nextPoint.x < 0 || nextPoint.y < 0)
                    {
                        continue;
                    }

                    if (nextPoint.x >= _tilesEnumerator.Size || nextPoint.y >= _tilesEnumerator.Size)
                    {
                        continue;
                    }
                    
                    if (stepsMap.TryGet(nextPoint, out var step))
                    {
                        if (step == stepNumber - 1)
                        {
                            return BuildPath(ref waypoints, ref stepsMap, stepNumber - 1, nextPoint);
                        }
                    }
                }
            }

            return false;
        }
    }
}