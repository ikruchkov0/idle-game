﻿using Unity.Mathematics;

namespace Pathfinding
{
    public readonly struct TilesEnumerator
    {
        private readonly TilesMap _tilesMap;

        public TilesEnumerator(in TilesMap tilesMap)
        {
            _tilesMap = tilesMap;
        }

        public int Size => _tilesMap.Size;

        public void Mark(
            ref StepsMap stepsMap,
            in int stepNumber,
            in int2 point)
        {
            for (int deltaI = -1; deltaI <= 1; deltaI++)
            {
                for (int deltaJ = -1; deltaJ <= 1; deltaJ++)
                {
                    var tI = point.x + deltaI;
                    var tJ = point.y + deltaJ;
                    var nextPoint = new int2(tI, tJ);

                    if (!IsTransitionPossible(point, nextPoint))
                    {
                        continue;
                    }

                    var nextStepNumber = stepNumber + 1;
                    
                    stepsMap.MarkIfLessStepNumber(nextPoint, nextStepNumber);
                }
            }
        }
        
        private bool IsTransitionPossible(
            int2 point,
            int2 nextPoint)
        {
            if (nextPoint.x < 0 || nextPoint.y < 0)
            {
                return false;
            }

            if (nextPoint.x >= Size || nextPoint.y >= Size)
            {
                return false;
            }
            
            if (_tilesMap[nextPoint] == TileType.Obstacle)
            {
                return false;
            }

            if (!IsDiagonalTransitionPossible(point, nextPoint))
            {
                return false;
            }

            return true;
        }

        private readonly bool IsDiagonalTransitionPossible(
            int2 point,
            int2 nextPoint)
        {
            if (!IsDiagonal(point, nextPoint))
            {
                return true;
            }

            if (nextPoint.x > 0 && nextPoint.y < Size)
            {
                var p = new int2(nextPoint.x, point.y);

                if (_tilesMap[p] == TileType.Obstacle)
                {
                    return false;
                }
            }


            if (nextPoint.y > 0 && nextPoint.y < Size)
            {
                var p = new int2(point.x, nextPoint.y);

                if (_tilesMap[p] == TileType.Obstacle)
                {
                    return false;
                }
            }

            return true;
        }


        private static bool IsDiagonal(
            int2 point,
            int2 nextPoint)
        {
            return math.abs(nextPoint.x - point.x) == 1 
                   && math.abs(nextPoint.y - point.y) == 1;
        }
    }
}