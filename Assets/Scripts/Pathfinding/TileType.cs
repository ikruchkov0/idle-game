﻿namespace Pathfinding
{
    public enum TileType: uint
    {
        Ground = 0,
        Obstacle = 1
    }
}