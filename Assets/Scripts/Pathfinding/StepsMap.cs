﻿using System;
using Unity.Collections;
using Unity.Mathematics;

namespace Pathfinding
{
    public  struct StepsMap: IDisposable
    {
        private readonly int _fieldSize;
        private NativeArray<StepInfo> _tilesMap;

        public StepsMap(
            in Allocator allocator,
            in int fieldSize)
        {
            _fieldSize = fieldSize;
            _tilesMap = new NativeArray<StepInfo>(fieldSize * fieldSize, allocator);
        }

        public int Size => _fieldSize;
        
        public readonly bool TryGet(int2 point, out int stepsNumber)
        {
            stepsNumber = -1;
            var current = _tilesMap[GetKey(point)];
            if (!current.IsMarked)
            {
                return false;
            }

            stepsNumber = current.StepNumber;
            return true;
        }

        public bool IsMarked(int2 point) => TryGet(point, out _);
        
        public void MarkIfLessStepNumber(int2 point, int stepNumber)
        {
            var key = GetKey(point);
            var oldInfo = _tilesMap[key];

            if (oldInfo.IsMarked && oldInfo.StepNumber < stepNumber)
            {
                return;
            }
            
            _tilesMap[key] = new StepInfo(stepNumber);
        }
        
        public void Dispose()
        {
            if (_tilesMap.IsCreated)
            {
                _tilesMap.Dispose();
            }
        }

        private readonly int GetKey(int2 point)
        {
            return GetKey(point.x, point.y);
        }

        private readonly int GetKey(int i, int j)
        {
            return j * _fieldSize + i;
        }

        private readonly struct StepInfo
        {
            public readonly bool IsMarked;
            public readonly int StepNumber;
            
            public StepInfo(int stepNumber)
            {
                IsMarked = true;
                StepNumber = stepNumber;
            }
        }
    }
}