﻿using Unity.Entities;
using Unity.Mathematics;

namespace Tiles
{
    public readonly struct TilePositionComponent : IComponentData
    {
        public readonly int2 Pos;

        public int I => Pos.x;

        public int J => Pos.y;

        public TilePositionComponent(int i, int j): this(new int2(i, j))
        {
        }

        public TilePositionComponent(int2 pos)
        {
            Pos = pos;
        }
        
    }
}
