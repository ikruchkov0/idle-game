﻿using Unity.Entities;
using Unity.Mathematics;

namespace Tiles
{
    public readonly struct TileStaticOffsetComponent : IComponentData
    {
        public readonly float3 Offset;

        public TileStaticOffsetComponent(float x, float y, float z)
        {
            Offset = new float3(x, y , z);
        }
        
        public TileStaticOffsetComponent(in float3 offset)
        {
            Offset = offset;
        }
    }
}