﻿using Unity.Entities;
using Unity.Mathematics;

namespace Tiles
{
    public readonly struct TileDynamicOffsetComponent : IComponentData
    {
        public readonly float3 Offset;
        
        public TileDynamicOffsetComponent(float3 offset)
        {
            Offset = offset;
        }

        public TileDynamicOffsetComponent(float x, float y, float z)
        {
            Offset = new float3(x, y , z);
        }
    }
}