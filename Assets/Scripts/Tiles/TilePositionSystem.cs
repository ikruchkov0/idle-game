﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Tiles
{
    public class TilePositionSystem : SystemBase
    {
        private const float StartX = 0.5f;
        private const float StartZ = 0.5f;
        private const float StaticY = -5f;
        private const float BlockSize = 1f;
        
        protected override void OnUpdate()
        {
            Entities
                .WithNone<TileStaticOffsetComponent>()
                .WithNone<TileDynamicOffsetComponent>()
                .ForEach((ref Translation t, in TilePositionComponent pos) =>
                {
                    t.Value = CalculatePosition(pos);
                })
                .ScheduleParallel();
            
            Entities
                .WithNone<TileDynamicOffsetComponent>()
                .ForEach((ref Translation t, in TilePositionComponent pos, in TileStaticOffsetComponent staticOffset) =>
                {
                    var tilePosition = CalculatePosition(pos);
                    t.Value = tilePosition + staticOffset.Offset;
                })
                .ScheduleParallel();
            
            Entities
                .ForEach((ref Translation t, in TilePositionComponent pos, in TileStaticOffsetComponent staticOffset, in  TileDynamicOffsetComponent dynamicOffset) =>
                {
                    var tilePosition = CalculatePosition(pos);
                    t.Value = tilePosition + staticOffset.Offset + dynamicOffset.Offset;
                })
                .ScheduleParallel();
        }

        private static float3 CalculatePosition(in TilePositionComponent pos) => CalculatePosition(pos.I, pos.J);

        public static float3 CalculatePosition(int i, int j)
        {
            return new float3(StartX + BlockSize * i, StaticY, StartZ + BlockSize * j);
        }
        
        public static float3 CalculatePosition(int2 pos)
        {
            return CalculatePosition(pos.x, pos.y);
        }
        
        public static int2 CalculateTilePos(float3 pos)
        {
            var i = (int)math.round((pos.x - StartX) / BlockSize);
            var j = (int)math.round((pos.z - StartZ) / BlockSize);
            return new int2(i, j);
        }
    }
}