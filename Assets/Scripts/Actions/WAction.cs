﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Actions
{
    public readonly struct WAction : IBufferElementData
    {
        public readonly int SequenceNumber;

        public readonly WActionType ActionType;

        public readonly int2 Target;

        private readonly FunctionPointer<ActionDelegate> _start;
        
        private readonly FunctionPointer<ActionDelegate> _end;

        private WAction(in int sequenceNumber, in WActionType actionType, in int2 target,
            in FunctionPointer<ActionDelegate> start,
            in FunctionPointer<ActionDelegate> end)
        {
            SequenceNumber = sequenceNumber;
            ActionType = actionType;
            Target = target;
            _start = start;
            _end = end;
        }

        public void Start(
            ref EntityCommandBuffer.ParallelWriter ecb,
            in int entityInQueryIndex,
            in Entity entity)
        {
            _start.Invoke(ref ecb, entityInQueryIndex, entity, this);
        }
        
        public void End(
            ref EntityCommandBuffer.ParallelWriter ecb,
            in int entityInQueryIndex,
            in Entity entity)
        {
            _end.Invoke(ref ecb, entityInQueryIndex, entity, this);
        }
        
        public static WAction CreateHeading(in NativeHashMap<uint, ActionDelegates> actionDelegates, in int sequenceNumber, in int2 pos)
            => Create(actionDelegates, sequenceNumber, WActionType.Heading, pos);

        public static WAction CreateWaypoint(in NativeHashMap<uint, ActionDelegates> actionDelegates, in int sequenceNumber, in int2 pos)
            => Create(actionDelegates, sequenceNumber, WActionType.Waypoint, pos);

        private static WAction Create(
            in NativeHashMap<uint, ActionDelegates> actionDelegates,
            int sequenceNumber,
            in WActionType actionType, int2 pos
            )
            => new WAction(sequenceNumber, actionType, pos,
                GetStartAction(actionType, actionDelegates),
                GetEndAction(actionType, actionDelegates));
        
        private static FunctionPointer<ActionDelegate> GetStartAction(WActionType actionType, in NativeHashMap<uint, ActionDelegates> actionDelegates)
        {
            return actionDelegates[(uint) actionType].Start;
        }
        
        private static FunctionPointer<ActionDelegate> GetEndAction(WActionType actionType, in NativeHashMap<uint, ActionDelegates> actionDelegates)
        {
            return actionDelegates[(uint) actionType].End;
        }
    }
}