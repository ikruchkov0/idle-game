﻿using Unity.Entities;

namespace Actions
{
    public readonly struct CurrentAction: IComponentData
    {
        public readonly WAction Current;

        public CurrentAction(WAction current)
        {
            Current = current;
        }
        
        
    }
}