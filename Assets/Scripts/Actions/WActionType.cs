﻿namespace Actions
{
    public enum WActionType: uint
    {
        None = 0,
        Waypoint = 1,
        Heading = 2
    }
}