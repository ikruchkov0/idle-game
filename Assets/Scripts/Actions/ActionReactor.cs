﻿using AOT;
using Heading;
using Hybrid.Navigation;
using Tiles;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Actions
{
    public delegate void ActionDelegate(
        ref EntityCommandBuffer.ParallelWriter ecb,
        in int entityInQueryIndex,
        in Entity entity,
        in WAction action);

    public readonly struct ActionDelegates
    {
        public readonly FunctionPointer<ActionDelegate> Start;
        public readonly FunctionPointer<ActionDelegate> End;

        public ActionDelegates(FunctionPointer<ActionDelegate> start, FunctionPointer<ActionDelegate> end)
        {
            Start = start;
            End = end;
        }
    }
    
    public static class ActionDelegatesFactory
    {
        public static void PopulateDelegates(ref NativeHashMap<uint, ActionDelegates> delegates)
        {
            if (!delegates.TryAdd((uint) WActionType.Waypoint, new ActionDelegates(
                BurstCompiler.CompileFunctionPointer<ActionDelegate>(Reactors.StartMovementTarget),
                BurstCompiler.CompileFunctionPointer<ActionDelegate>(Reactors.EndMovementTarget)
            )))
            {
                Debug.Log($"Key {WActionType.Waypoint} found already added");
            }
            
            if (!delegates.TryAdd((uint) WActionType.Heading,new ActionDelegates(
                BurstCompiler.CompileFunctionPointer<ActionDelegate>(Reactors.StartHeadingTarget),
                BurstCompiler.CompileFunctionPointer<ActionDelegate>(Reactors.EndHeadingTarget)
            )))
            {
                Debug.Log($"Key {WActionType.Heading} found already added");
            }
        }
    }

    [BurstCompile]
    public static class Reactors
    {
        [BurstCompile]
        [MonoPInvokeCallback(typeof(ActionDelegate))]
        public static void StartMovementTarget(
            ref EntityCommandBuffer.ParallelWriter ecb,
            in int entityInQueryIndex,
            in Entity entity,
            in WAction action)
        {
            //Debug.Log("StartMovementTarget");
            //ecb.AddComponent(entityInQueryIndex, entity, new MovementTargetComponent(action));
            ecb.AddComponent(entityInQueryIndex, entity, new NavAgentTargetComponent(action.Target));
        }
        
        [BurstCompile]
        [MonoPInvokeCallback(typeof(ActionDelegate))]
        public static void EndMovementTarget(
            ref EntityCommandBuffer.ParallelWriter ecb,
            in int entityInQueryIndex,
            in Entity entity,
            in WAction action)
        {
            //Debug.Log("EndMovementTarget");
            //ecb.RemoveComponent<MovementTargetComponent>(entityInQueryIndex, entity);
            //ecb.RemoveComponent<TileDynamicOffsetComponent>(entityInQueryIndex, entity);
            ecb.SetComponent(entityInQueryIndex, entity, new TilePositionComponent(action.Target));
        }
        
        [BurstCompile]
        [MonoPInvokeCallback(typeof(ActionDelegate))]
        public static void StartHeadingTarget(
            ref EntityCommandBuffer.ParallelWriter ecb,
            in int entityInQueryIndex,
            in Entity entity,
            in WAction action)
        {
            //Debug.Log("StartHeadingTarget");
            ecb.AddComponent(entityInQueryIndex, entity, new HeadingTargetComponent(action));
        }
        
        [BurstCompile]
        [MonoPInvokeCallback(typeof(ActionDelegate))]
        public static void EndHeadingTarget(
            ref EntityCommandBuffer.ParallelWriter ecb,
            in int entityInQueryIndex,
            in Entity entity,
            in WAction action)
        {
            //Debug.Log("EndHeadingTarget");
            ecb.RemoveComponent<HeadingTargetComponent>(entityInQueryIndex, entity);
        }
    }
}