﻿using Unity.Entities;

namespace Actions
{
    public class ActionSystem: SystemBase
    {
        EndSimulationEntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            _commandBufferSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            base.OnCreate();
        }
        
        protected override void OnUpdate()
        {
            var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();
            
            Entities
                .WithNone<CurrentAction>()
                .ForEach((
                    ref DynamicBuffer<WAction> actions,
                    in int entityInQueryIndex,
                    in Entity entity
                ) =>
                {
                    if (actions.Length == 0)
                    {
                        return;
                    }

                    var currentAction = actions[0];
                    
                    currentAction.Start(ref ecb, entityInQueryIndex, entity);
                    
                    ecb.AddComponent(entityInQueryIndex, entity, new CurrentAction(actions[0]));

                    if (HasComponent<IdleComponent>(entity))
                    {
                        ecb.RemoveComponent<IdleComponent>(entityInQueryIndex, entity);
                    }
                })
                .ScheduleParallel();
            
            Entities
                .WithAll<FinishedAction>()
                .ForEach((
                    ref DynamicBuffer<WAction> actions,
                    ref CurrentAction currentAction,
                    in int entityInQueryIndex,
                    in Entity entity
                ) =>
                {
                    currentAction.Current.End(ref ecb, entityInQueryIndex, entity);
                    
                    ecb.RemoveComponent<FinishedAction>(entityInQueryIndex, entity);
                    ecb.RemoveComponent<CurrentAction>(entityInQueryIndex, entity);
                    
                    var currentActionIndex = -1;
                    for (var i = 0; i < actions.Length; i++)
                    {
                        if (actions[i].SequenceNumber == currentAction.Current.SequenceNumber)
                        {
                            currentActionIndex = i;
                            break;
                        }
                    }

                    if (currentActionIndex == -1)
                    {
                        return;
                    }
                    
                    actions.RemoveAt(currentActionIndex);

                    if (actions.Length == 0)
                    {
                        ecb.AddComponent(entityInQueryIndex, entity, new IdleComponent());
                    }
                    
                })
                .ScheduleParallel();
            
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}