using System;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class RigTest : MonoBehaviour
{
    void OnMouseDown() {
        Debug.Log(name);

        var chr = GameObject.Find("/Character");
        if (chr == null)
        {
            throw new Exception("Character not found");
        }
        
        var lHandle = GameObject.Find("/LeftHandHandle");
        if (lHandle == null)
        {
            throw new Exception("LeftHandHandle not found");
        }
        
        var lPosition = GameObject.Find("/Character/Rig 1/LeftHandPosition");
        if (lPosition == null)
        {
            throw new Exception("LeftHandPosition not found");
        }

        var lPositionConstraint = lPosition.GetComponent<TwoBoneIKConstraint>();
        lPositionConstraint.weight = 0.0f;
        
        var rPosition = GameObject.Find("/Character/Rig 1/RightHandPosition");
        if (rPosition == null)
        {
            throw new Exception("RightHandPosition not found");
        }
        
        var rPositionConstraint = rPosition.GetComponent<TwoBoneIKConstraint>();
        rPositionConstraint.weight = 0.0f;
        
        var hPosition = GameObject.Find("/Character/Rig 1/HeadPosition");
        if (hPosition == null)
        {
            throw new Exception("HeadPosition not found");
        } 
        
        var hPositionConstraint = hPosition.GetComponent<MultiAimConstraint>();
        hPositionConstraint.weight = 0.0f;

        var lTarget = GameObject.Find("/Character/Rig 1/LeftHandPosition/Target");
        if (lTarget == null)
        {
            throw new Exception("LeftHandTarget not found");
        }
        
        var animator = chr.GetComponent<Animator>();
        if (animator.GetCurrentAnimatorStateInfo(0).shortNameHash == Animator.StringToHash("Rummaging"))
        {
            animator.SetTrigger("Idle");
            return;
        }
        
        var hTarget = GameObject.Find("/Character/Rig 1/HeadPosition/Target");
        if (hTarget == null)
        {
            throw new Exception("HeadTarget not found");
        }
        
        var rTarget = GameObject.Find("/Character/Rig 1/RightHandPosition/Target");
        if (rTarget == null)
        {
            throw new Exception("RightHandTarget not found");
        }
        
        var offset = new Vector3(0, 0.07f, 0.06f);

        var objectPosition = transform.position + offset;

        lTarget.transform.position = lHandle.transform.position;

        rTarget.transform.position = objectPosition;
        hTarget.transform.position = objectPosition;
        
        animator.SetTrigger("Rummaging");
        lPositionConstraint.weight = 1.0f;
        rPositionConstraint.weight = 1.0f;
        hPositionConstraint.weight = 1.0f;
    }
}
